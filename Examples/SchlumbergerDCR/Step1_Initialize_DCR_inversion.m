% Generates the structure needed to run a trans-dimensional MCMC inversion
% of Schlumberger DC-resistivity data using PT_RJMCMC.m

clear
close all
clc

FileNameRoot = 'SchlumbergerDCR';
FileName = ['Trash/',FileNameRoot,'.mat'];

%specify which data types (surface-towed CSEM, ocean-bottom CSEM, MT) to include
ST = false;
OB = false; 
MT = false;
DCR = true;

% this array is used to determine which data types to compute forward
% responses for during the Bayesian inversion
S.dataTypes = false(4,1); 
S.dataTypes(1) = ST;
S.dataTypes(2) = OB;
S.dataTypes(3) = MT;
S.dataTypes(4) = DCR;

% set these to false, since we're doing trans-D MCMC
S.fixedDimension = false;
S.fixedWater = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load in the Schlumberger data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data=load ('data.txt');
% set the minimum error to 10%
idx = data(:,3)./data(:,2) < 0.1;
data(idx,3) = 0.1*data(idx,2);
% omit two noisy data points
range = [1:25 28:31];  
S.dcrDat.appRes = log10(data(range,2));  % log apparent resistivity, ohm-m
S.dcrDat.es = data(range,1); % electrode spacing, meters
S.dcrDat.appResErr = 0.4343*data(range,3)./data(range,2); % apparent resistivity errors, log domain

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MCMC parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%general parameters
S.debug_prior = false; % only sample prior if true. For testing the prior and the forward calls
S.transform01_ab = false; % if you want to use  (0,1) prior, then transform to (a,b)
if( S.transform01_ab == true )
   % load maximum allowed resistivity as a function of depth
end
S.logZ = true;
S.regionBoundaryDepth = 0.1; % (m); this is a land data set, we're ignoring the water layer (S1)
S.nTemps = 6;         % number of temperatures in PT
nChainsAt1 = 2;       % number of chains at T=1 (increasing this adds more models to the ensemble)
S.nChains = S.nTemps + nChainsAt1; %total number of chains in PT (possibly multiple chains at T=1)
S.Tmax = 2.0;         % Tmax for PT
S.B = [ logspace(-log10(S.Tmax),0,S.nTemps) ones(1,S.nChains-S.nTemps) ]; %inverse temperature ladder
S.numIterations = 1d5;  % 100,000 Number of RJ-MCMC iterations to carry out (should be at least 100,000)
S.saveEvery     = 1d6;   % How often to write the models in the chain to an output file
S.displayEvery  = 1d3;  % How often to display progress to the command window


%This code divides the model space into a water column portion and a
%subsurface portion. This section sets specific parameters for the water column portion of the model
S1.isotropic = true;   % This code is currently isotropic only (this parameter must be 'true')
S1.birth_death_from_prior = true; % proposal distribution = prior for birth/death moves
S1.kInit = 1;          % number of interfaces in the random initial model
S1.kMin = 1 ;          % Minimum number of additional layer interfaces -- 1 is the min allowed here
S1.kMax = 7;           % Maximum number of additional layer interfaces
S1.zMin = 0.01;   % There is no min thickness, but transmitter and receivers must be in same layer
S1.zMax = S.regionBoundaryDepth; % Max depth interfaces can be placed at in the water column (should be less than Tx tow depth 
S1.rhoMin = 1;     % Minimum log-resistivity of the uniform prior distribution (for water column)
S1.rhoMax = 2;     % Maximum log-resistivity of the uniform prior distribution (for water column)
%These step sizes are for the MCMC model space search. Tuning these can be
%a pain, but they only affect rate of convergence, not convergence itself
S1.UstepSize = [ linspace(0.02, 0.002, S.nTemps) 0.002*ones(1,S.nChains-S.nTemps) ]; %update step
S1.BstepSize = [ linspace(0.05, 0.025, S.nTemps) 0.025*ones(1,S.nChains-S.nTemps) ]; %birth step
S1.MstepSize = [ linspace(7, 2, S.nTemps) 2*ones(1,S.nChains-S.nTemps) ]; %move interface step


%This code divides the model space into a water column portion and a
%subsurface portion. This section sets specific parameters for the subsurface portion of the model
S2.isotropic = true;   % This code is currently isotropic only (this parameter must be 'true')
S2.kInit = 1;          % number of interfaces in the random initial model
S2.kMin = 1 ;           % Minimum number of additional layer interfaces
S2.kMax = 40;          % Maximum number of additional layer interfaces
S2.zMin = S.regionBoundaryDepth;   % There is no min thickness, but transmitter and receivers must be in same layer
S2.zMax = 1.85e5; % depth at which to place last interface
S2.rhoMin = -1;  % Minimum log-resistivity of the uniform prior distribution (for subsurface)
S2.rhoMax = 6;   % Maximum log-resistivity of the uniform prior distribution (for subsurface)
%These step sizes are for the MCMC model space search. Tuning these can be
%a pain, but they only affect rate of convergence, not convergence itself
S2.UstepSize = [ linspace(0.06, 0.01, S.nTemps) 0.01*ones(1,S.nChains-S.nTemps) ]; %for update in log10 rho
S2.BstepSize = [ linspace(0.6, 0.4, S.nTemps) 0.4*ones(1,S.nChains-S.nTemps) ]; %for birth / death in log10 rho
S2.MstepSize = [ linspace(25, 7, S.nTemps) 7*ones(1,S.nChains-S.nTemps) ]; %for move interface in m

if( S.logZ == true )
   S1.zMin = log10(S1.zMin);
   S1.zMax = log10(S1.zMax);
   S1.MstepSize = log10(S1.MstepSize);
   S2.zMin = log10(S2.zMin);
   S2.zMax = log10(S2.zMax);
   S.regionBoundaryDepth = log10(S.regionBoundaryDepth);
   S2.MstepSize = log10(S2.MstepSize);
end

S.zMax = S2.zMax;

%beta determines what fraction of the time we do an MCMC step for the water
%column portion of the model. (1-beta): how often we do a subsurface MCMC step
S.beta = 0.0; 

%%%%%%%%%%%%%%%%%%%%%
%%% Plot the data
%%%%%%%%%%%%%%%%%%%%%

figure
errorbar(S.dcrDat.es, S.dcrDat.appRes, 2*S.dcrDat.appResErr, 'ro'); 
xlabel('electrode spacing (m)')
ylabel('log(apparent resistivity) (ohm-m)')
set(gca,'XScale','log')
set(gca,'FontSize',14)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save the MCMC input files
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(FileName,'-struct','S')
save('Trash/WaterColumn.mat','-struct','S1')
save('Trash/Subsurface.mat','-struct','S2')






