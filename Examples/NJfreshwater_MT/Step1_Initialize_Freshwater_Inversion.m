% Generates the structure needed to run a trans-dimensional MCMC inversion
% of ocean-bottom CSEM, surface-towed CSEM, and MT data (any or all of the
% above) using PT_RJMCMC.m

clear all
clc

%specify which data types (surface-towed CSEM, ocean-bottom CSEM, MT) to include
ST = false;
OB = false; 
MT = true;
DCR = false;

% this array is used to determine which data types to compute forward
% responses for during the Bayesian inversion
S.dataTypes = false(4,1); 
S.dataTypes(1) = ST;
S.dataTypes(2) = OB;
S.dataTypes(3) = MT;
S.dataTypes(4) = DCR;

MTnum = 5; %specifies the MT station to invert, if ST == false
LineNum = 1;

S.fixedDimension = false;
S.fixedWater = false;

% -------------------------------------------------------------------
% Unless you're modifying the code, you shouldn't have to worry about
% anything below here
% -------------------------------------------------------------------

load('Examples/NJfreshwater_Joint_MT_stCSEM/Line1_MT_Data.mat')


%We don't want to use MT data longer than 10s period, due to wave noise and
%the fact that we're interested in shallow structure (towed CSEM system is
%only sensitive to upper km or so)
PeriodCut = 10;
ind = MTnum;
ind2 = find(1./MTfreqs{ind} < PeriodCut,1,'last');

%pick out only those frequencies that are shorter than PeriodCut (in sec)
S.MTdat.freqs = MTfreqs{ind}(1:ind2); %allfreqs; 
S.MTdat.TEappRes = TEappRes{ind}(1:ind2); %allAppRes; 
S.MTdat.TEappResErr = TEappResErr{ind}(1:ind2); %allAppResErr;
S.MTdat.TEphase = TEphase{ind}(1:ind2); %allPhase; 
S.MTdat.TEphaseErr = TEphaseErr{ind}(1:ind2); %allPhaseErr; 


%RJMCMC parameters:

%general parameters
S.debug_prior = false; % only sample prior if true. For testing the prior and the forward calls
S.transform01_ab = false; % if you want to use  (0,1) prior, then transform to (a,b)
if( S.transform01_ab == true )
   % load maximum allowed resistivity as a function of depth

end
S.logZ = false;
S.nTemps = 6;         % number of temperatures in PT
S.regionBoundaryDepth = 0.1;
nChainsAt1 = 2;       % number of chains at T=1 (increasing this adds more models to the ensemble)
S.nChains = S.nTemps + nChainsAt1; %total number of chains in PT (possibly multiple chains at T=1)
S.Tmax = 2.0;         % Tmax for PT
S.B = [ logspace(-log10(S.Tmax),0,S.nTemps) ones(1,S.nChains-S.nTemps) ]; %inverse temperature ladder
S.numIterations = 1.5d5;  % 100,000 Number of RJ-MCMC iterations to carry out (should be at least 100,000)
S.saveEvery     = 1.5d5;   % How often to write the models in the chain to an output file
S.displayEvery  = 1d3;  % How often to display progress to the command window


%This code divides the model space into a water column portion and a
%subsurface portion. This section sets specific parameters for the water column portion of the model
S1.isotropic = true;   % This code is currently isotropic only (this parameter must be 'true')
S1.birth_death_from_prior = true; % proposal distribution = prior for birth/death moves
S1.kInit = 1;          % number of interfaces in the random initial model
S1.kMin = 1 ;          % Minimum number of additional layer interfaces -- 1 is the min allowed here
S1.kMax = 7;           % Maximum number of additional layer interfaces
S1.zMin = 0.01;   % There is no min thickness, but transmitter and receivers must be in same layer
S1.zMax = S.regionBoundaryDepth; % An interface is automatically placed at zMax, and should be the water depth 
S1.rhoMin = -0.75;     % Minimum log-resistivity of the uniform prior distribution (for water column)
S1.rhoMax = -0.55;     % Maximum log-resistivity of the uniform prior distribution (for water column)
%These step sizes are for the MCMC model space search. Tuning these can be
%a pain, but they only affect rate of convergence, not convergence itself
S1.UstepSize = [ linspace(0.02, 0.002, S.nTemps) 0.002*ones(1,S.nChains-S.nTemps) ]; %update step
S1.BstepSize = [ linspace(0.05, 0.025, S.nTemps) 0.025*ones(1,S.nChains-S.nTemps) ]; %birth step
S1.MstepSize = [ linspace(7, 2, S.nTemps) 2*ones(1,S.nChains-S.nTemps) ]; %move interface step

%This code divides the model space into a water column portion and a
%subsurface portion. This section sets specific parameters for the subsurface portion of the model
S2.isotropic = true;   % This code is currently isotropic only (this parameter must be 'true')
S2.kInit = 1;          % number of interfaces in the random initial model
S2.kMin = 1 ;           % Minimum number of additional layer interfaces
S2.kMax = 100;          % Maximum number of additional layer interfaces
S2.zMin = S.regionBoundaryDepth;   % There is no min thickness, but transmitter and receivers must be in same layer
S2.zMax = 2000; % depth at which to place last interface  
S2.rhoMin = -1;  % Minimum log-resistivity of the uniform prior distribution (for subsurface)
S2.rhoMax = 3;   % Maximum log-resistivity of the uniform prior distribution (for subsurface)
%These step sizes are for the MCMC model space search. Tuning these can be
%a pain, but they only affect rate of convergence, not convergence itself
S2.UstepSize = [ linspace(0.06, 0.01, S.nTemps) 0.0175*ones(1,S.nChains-S.nTemps) ]; %for update in log10 rho
S2.BstepSize = [ linspace(0.6, 0.4, S.nTemps) 0.4*ones(1,S.nChains-S.nTemps) ]; %for birth / death in log10 rho
S2.MstepSize = [ linspace(50, 7, S.nTemps) 15*ones(1,S.nChains-S.nTemps) ]; %for move interface in m

if( S.logZ == true )
    S.regionBoundaryDepth = log10(S.regionBoundaryDepth);
    S1.zMin = log10(S1.zMin);
    S1.zMax = log10(S1.zMax);
    S1.MstepSize = log10(S1.MstepSize);
    S2.zMin = log10(S2.zMin);
    S2.zMax = log10(S2.zMax);
    S2.MstepSize = log10(S2.MstepSize);
end

%beta determines what fraction of the time we do an MCMC step for the water
%column portion of the model. (1-beta): how often we do a subsurface MCMC step
S.beta = 0.0; 

%This determines the output file name (using the choice of data inverted)
%and writes the necessary output files. WaterColumn.mat and Subsurface.mat
%contain the parameters unique to these portions of the model. The other .mat
%file contains all the general parameters, as well as the data
FilePrefix = 'Trash/Line'; %1_Joint_ST';
if( ST == true && MT == true )
   invtype = 'Joint';
   FileName = [ FilePrefix LineNum '_' invtype '_ST' num2str(sBeg) '-' num2str(sEnd) '_MT' num2str(ind) '.mat' ];
elseif( ST == true && MT == false)
   invtype = 'STonly';
   FileName = [ FilePrefix LineNum '_' invtype '_ST' num2str(sBeg) '-' num2str(sEnd) '.mat' ];
elseif( ST == false && MT == true )
   invtype = 'MTonly';
   FileName = [ FilePrefix num2str(LineNum) '_' invtype '_MT' num2str(ind) '.mat' ];
elseif( ST == false && MT == false )
   display('you gotta invert something!!')
   return
end
%FileName = [ FilePrefix LineNum invtype '_ST' num2str(sBeg) '-' num2str(sEnd) '_MT' num2str(ind) '.mat' ];
save(FileName,'-struct','S')
save('Trash/WaterColumn.mat','-struct','S1')
save('Trash/Subsurface.mat','-struct','S2')


%This portion of the code plots the MT and CSEM data you chose to invert

%for plotting error bars
eAR = S.MTdat.TEappRes + S.MTdat.TEappResErr;
eAR = [ eAR ; S.MTdat.TEappRes - S.MTdat.TEappResErr ];
ePh = S.MTdat.TEphase + S.MTdat.TEphaseErr;
ePh = [ ePh ; S.MTdat.TEphase - S.MTdat.TEphaseErr ];

% Plot the response:
figure
subplot(2,1,1)
semilogx(1./S.MTdat.freqs,S.MTdat.TEappRes,'or','linewidth',2)
hold on
semilogx([1./S.MTdat.freqs;1./S.MTdat.freqs],eAR,'-k','linewidth',2)

title('Apparent Resistivity ')
ylabel('log_{10}(\rho) (ohm-m)')
set(gca,'fontsize',12)

subplot(2,1,2)
semilogx(1./S.MTdat.freqs,S.MTdat.TEphase,'or','linewidth',2)
hold on
semilogx([1./S.MTdat.freqs;1./S.MTdat.freqs],ePh,'-k','linewidth',2)
ax = axis;
% axis([ax(1) ax(2) 0 90]);
xlabel('Period (s)');
ylabel('degrees');
title('Phase')
set(gca,'fontsize',12)

% 
% %plot CSEM data
% figure(9)
% r = sqrt((S.stTx.X - S.stRx.X).^2 + (S.stTx.Y - S.stRx.Y).^2);
% r = mean(r,2);
% r = r';
% for ifreq=1:size(S.stDat.Er,1)
%    %make the error bars for plotting
%    eER = S.stDat.Er(ifreq,:) + S.stDat.ErErr(ifreq,:);
%    eER = [ eER ; S.stDat.Er(ifreq,:) - S.stDat.ErErr(ifreq,:) ];
%    ePh = S.stDat.Phase(ifreq,:) + S.stDat.PhaseErr(ifreq,:);
%    ePh = [ ePh ; S.stDat.Phase(ifreq,:) - S.stDat.PhaseErr(ifreq,:) ];
%    subplot(2,1,1)
%    plot(r,S.stDat.Er(ifreq,:),'or','linewidth',2)
%    hold on
%    plot([r;r],eER,'-k','linewidth',2)
%    ylabel('log(|E_r|) (V/Am)')
%    set(gca,'fontsize',14)
% 
%    subplot(2,1,2)
%    plot(r,S.stDat.Phase(ifreq,:),'or','linewidth',2)
%    hold on
%    plot([r;r],ePh,'-k','linewidth',2)
%    ylabel('Phase (^o)')
%    xlabel('Source-receiver offset (m)')
%    set(gca,'fontsize',14)
% end





