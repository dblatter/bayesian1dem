
clear all
clc

FileNameRoot = 'RIS_MT_A24';

%load in the MT data structure
load('Examples/RossIceShelf/A24_data.mat')
%output filename
FileName = ['Trash/',FileNameRoot,'.mat'];
%there is something wrong with reading in the NANS
%select either TE or TM mode
TE = false;
TM = true;
if( TE == true && TM == true )
   fprintf('we are not set up to invert both TE and TM modes simultaneously! Sorry.\n')
   exit
end

%pcntErr = 1.0; % 1.0 corresponds to using the data error in MTdata.mat, which is 10%
phaseErrFloor = 0.7; % error floor in degrees for phase
appResErrFloor = 0.04; % error floor (as percent of measured data) for apparent resistivity


iceThick = 0.5; %ice thickness CHLOE CHLOE CHLOE DON'T FORGET THIS 

%pick out the desired data (TM apparent resistivity and phase)
k = 1; %TM mode apparent resistivity counter
l = 1; %TM mode phase counter
for j=1:length(MTdata.DatID)
   if( TM == true )
      if( MTdata.DatID(j) == 125 ) %TM mode apparent resistivity ID is 125
%          S.MTdat.TEappRes(k) = log10(MTdata.Data(j)); %ignore the 'TE' here
         S.MTdat.TEappRes(k) = MTdata.Data(j);
%          S.MTdat.TEappResErr(k) = ( MTdata.DataErr(j)./MTdata.Data(j) );%*(1/log(10))*(pcntErr);
         S.MTdat.TEappResErr(k) = MTdata.DataErr(j);
         if( S.MTdat.TEappResErr(k) < (1/log(10)) * appResErrFloor )
             S.MTdat.TEappResErr(k) = (1/log(10)) * appResErrFloor;
         end
         S.MTdat.freqs(k) = MTdata.Freqs(MTdata.FreqID(j));
         k = k + 1;
      elseif( MTdata.DatID(j) == 106 ) %TM mode phase ID is 106
         S.MTdat.TEphase(l) = MTdata.Data(j);
         S.MTdat.TEphaseErr(l) = MTdata.DataErr(j);
         if( S.MTdat.TEphaseErr(l) < phaseErrFloor ) % a 1 degree error floor on phase
            S.MTdat.TEphaseErr(l) = phaseErrFloor; 
         end
         l = l + 1;
      end
   elseif( TE == true )
      if( MTdata.DatID(j) == 123 ) %TE mode apparent resistivity ID is 123
         S.MTdat.TEappRes(k) = MTdata.Data(j); %ignore the 'TE' here
         S.MTdat.TEappResErr(k) = MTdata.DataErr(j); %*(1/log(10))*(pcntErr);
         if( S.MTdat.TEappResErr(k) < (1/log(10)) * appResErrFloor )
             S.MTdat.TEappResErr(k) = (1/log(10)) * appResErrFloor;
         end
         S.MTdat.freqs(k) = MTdata.Freqs(MTdata.FreqID(j));
         k = k + 1;
      elseif( MTdata.DatID(j) == 104 ) %TE mode phase ID is 104
         S.MTdat.TEphase(l) = MTdata.Data(j);
         S.MTdat.TEphaseErr(l) = MTdata.DataErr(j);
         if( S.MTdat.TEphaseErr(l) < phaseErrFloor ) % a 1 degree error floor on phase
            S.MTdat.TEphaseErr(l) = phaseErrFloor; 
         end
         l = l + 1;
      end
   else
      fprintf('You gotta invert something!\n')
      exit
   end
end

S.MTdat.TEphaseErr = phaseErrFloor*ones(size(S.MTdat.TEphaseErr));
S.MTdat.TEappResErr = (appResErrFloor/0.1)*S.MTdat.TEappResErr;

S.Tx.WaterDepth = MTdata.WaterDepth;
%S.regionBoundaryDepth = S.Tx.WaterDepth;
S.regionBoundaryDepth = iceThick;

%specify which data types (surface-towed CSEM, ocean-bottom CSEM, MT) to include
ST = false;
OB = false; 
MT = true;
DCR = false;

% this array is used to determine which data types to compute forward
% responses for during the Bayesian inversion
S.dataTypes = false(3,1); 
S.dataTypes(1) = ST;
S.dataTypes(2) = OB;
S.dataTypes(3) = MT;
S.dataTypes(4) = DCR;

%specify if you want a fixed-dimension problem (fixed number of interfaces
%that don't move around during the inversion). If this is set to true, you
%must define TrueModel.z as a field of S, which is an array of interfaces
%which assumes z = 0 is the sea surface, (not included in the array), 
%contains the seafloor (which must be equal to S.Tx.WaterDepth), and must 
%contain at least one interface above the seafloor. It also must contain at
%least one interface below the seafloor
S.fixedDimension = false;
%Additionally, if you don't want the water column inverted for either, set 
%this to true. If you do, you must also define S.TrueModel.rho, which contains 
%log10 resistivities, one each for the layer above the corresponding entry in z
%(you must also specify the terminating half-space, so rho will need one
%more entry than z).
S.fixedWater = false;

%Fixed interfaces for fixed-dimensional inversions
if( S.fixedDimension == true )
TrueModel.z = logspace(1.3,3.2,15);  
end

%RJMCMC parameters:

%general parameters
S.debug_prior = false; % only sample prior if true. For testing the prior and the forward calls
S.transform01_ab = false; % if you want to use  (0,1) prior, then transform to (a,b)
if( S.transform01_ab == true )
   % load maximum allowed resistivity
   A = load('Examples/SERPENT_MT/maxrho_1300C_MPT.mat');
   S.maxRho = A.maxrho'; % because now we're transforming to (a,b)
   S.minRho = -1*ones(1,length(S.maxRho));
   S.zRhoLim = A.z; % the depths for the maxRho and minRho arrays, above
   S.dz = 100; % depth binning, (m); keep this small relative to zMax 
   S.prior = 1./(S.maxRho - S.minRho)'; % for computing the KL divergence in Step 5
end
S.logZ = false; % whether or not to sample in log(z) rather than z %CDG changed to false
S.nTemps = 12;         % number of temperatures in PT
nChainsAt1 = 3;       % number of chains at T=1 (increasing this adds more models to the ensemble)
S.nChains = S.nTemps + nChainsAt1 - 1; %total number of chains in PT (possibly multiple chains at T=1)
S.Tmax = 2.2;         % Tmax for PT
S.B = [ logspace(-log10(S.Tmax),0,S.nTemps) ones(1,S.nChains-S.nTemps) ]; %inverse temperature ladder
S.numIterations = 6d4;  % 100,000 Number of RJ-MCMC iterations to carry out (should be at least 100,000)
S.saveEvery     = 6d4;  % How often to write the models in the chain to an output file
S.displayEvery  = 1d3;  % How often to display progress to the command window


%This code divides the model space into a water column portion and a
%subsurface portion. This section sets specific parameters for the water column portion of the model
S1.birth_death_from_prior = true; % proposal distribution = prior for birth/death moves
S1.isotropic = true;   % This code is currently isotropic only (this parameter must be 'true')
S1.kInit = 1;          % number of interfaces in the random initial model
S1.kMin = 1 ;          % Minimum number of additional layer interfaces -- 1 is the min allowed here
S1.kMax = 10;           % Maximum number of additional layer interfaces 
S1.zMin = 0.1;   % There is no min thickness, but transmitter and receivers must be in same layer
%S1.zMax = S.Tx.WaterDepth; % An interface is automatically placed at zMax, and should be the water depth CDG changed to ice thickness
S1.zMax = iceThick;% - 0.1; % CDG changed to ice thickness (not sure why the -0.1 is there. ASK DAN
if( S.logZ == true )
   S1.zMin = log10(S1.zMin);
   S1.zMax = log10(S1.zMax);
end
S1.rhoMin = -1;     % Minimum log-resistivity of the uniform prior distribution (for water column)
S1.rhoMax = 4;     % Maximum log-resistivity of the uniform prior distribution (for water column)

%These step sizes are for the MCMC model space search. Tuning these can be
%a pain, but they only affect rate of convergence, not convergence itself

%for update in log10 rho
s1u_min = 0.05;
s1u_max = 0.2; %CHECK SWAP RATES
%for birth / death in log10 rho (not used if birth_death_from_prior=true;)
s1b_min = 0.4;
s1b_max = 0.6;
%for move interface in meters
s1m_min = 100;
s1m_max = 1000;

S1.UstepSize = [ linspace(s1u_max, s1u_min, S.nTemps) s1u_min*ones(1,S.nChains-S.nTemps) ]; %update step
S1.BstepSize = [ linspace(s1b_max, s1b_min, S.nTemps) s1b_min*ones(1,S.nChains-S.nTemps) ]; %birth step
S1.MstepSize = [ linspace(s1m_max, s1m_min, S.nTemps) s1m_min*ones(1,S.nChains-S.nTemps) ]; %move interface step
if( S.logZ == true )
   S1.MstepSize = [ linspace(s1m_max, s1m_min, S.nTemps) s1m_min*ones(1,S.nChains-S.nTemps) ];
   S1.MstepSize = log10(S1.MstepSize);
end

%This code divides the model space into a water column portion and a
%subsurface portion. This section sets specific parameters for the subsurface portion of the model
S2.birth_death_from_prior = true; % proposal distribution = prior for birth/death moves
S2.isotropic = true;   % This code is currently isotropic only (this parameter must be 'true')
S2.kInit = 1;          % number of interfaces in the random initial model
S2.kMin = 1 ;           % Minimum number of additional layer interfaces
S2.kMax = 75;          % Maximum number of additional layer interfaces
S2.zMin = iceThick;% + 0.1;   % can't remember why there is a +1 here. ASK DAN
S2.zMax = 1.5e4; % max depth at which the algorithm can place an interface  
if( S.logZ == true )
   S2.zMin = log10(S2.zMin);
   S2.zMax = log10(S2.zMax);
   S.regionBoundaryDepth = log10(S.regionBoundaryDepth);
end
S2.rhoMin = -1;  % Minimum log-resistivity of the uniform prior distribution (for subsurface)
S2.rhoMax = 4;   % Maximum log-resistivity of the uniform prior distribution (for subsurface)
if( S.transform01_ab == true )
   S2.rhoMin = 0; % because now we're proposing from (0,1)
   S2.rhoMax = 1;
   S.zMin = S2.zMin;
   S.zMax = S2.zMax;
else
   S.prior = 1/(S2.rhoMax-S2.rhoMin);
end
%These step sizes are for the MCMC model space search. Tuning these can be
%a pain, but they only affect rate of convergence, not convergence itself

%for update in log10 rho
s2u_min = 0.01; %these will need tuning (look at acceptance rate (between 20 - 50%))
s2u_max = 0.1;
%for birth / death in log10 rho
s2b_min = 0.4;
s2b_max = 0.6;
%for move interface in meters
s2m_min = 100;
s2m_max = 1000;

S2.UstepSize = [ linspace(s2u_max, s2u_min, S.nTemps) s2u_min*ones(1,S.nChains-S.nTemps) ]; %for update in log10 rho
S2.BstepSize = [ linspace(s2b_max, s2b_min, S.nTemps) s2b_min*ones(1,S.nChains-S.nTemps) ]; %for birth / death in log10 rho
S2.MstepSize = [ linspace(s2m_max, s2m_min, S.nTemps) s2m_min*ones(1,S.nChains-S.nTemps) ]; %for move interface in m
if( S.logZ == true )
   S2.MstepSize = [ linspace(s2m_max, s2m_min, S.nTemps) s2m_min*ones(1,S.nChains-S.nTemps) ];
   S2.MstepSize = log10(S2.MstepSize);
end

%beta determines what fraction of the time we do an MCMC step for the water
%column portion of the model. (1-beta): how often we do a subsurface MCMC step
S.beta = 0.01; 

%Set true model, for fixed-dimensional inversions. Make sure S2.kInit
%matches the number of interfaces in S.TrueModel.z
if( S.fixedDimension == true )
   S.TrueModel.z = [ S.Tx.WaterDepth/2 S.Tx.WaterDepth (S.Tx.WaterDepth+TrueModel.z) ];
   %S.TrueModel.z = [ S.Tx.WaterDepth/2 S.Tx.WaterDepth TrueModel.z ];
end
%if water column is fixed, specify its resistivity here
if( S.fixedWater == true )
   rhoWater = (S1.rhoMin + S1.rhoMax)/2;
   %only the water column resistivities will be used
   S.TrueModel.rho = [ rhoWater rhoWater 2 2 2 2 2 2 ];
end

%plot the data!

%for plotting error bars
eAR = S.MTdat.TEappRes + S.MTdat.TEappResErr;
eAR = [ eAR ; S.MTdat.TEappRes - S.MTdat.TEappResErr ];
ePh = S.MTdat.TEphase + S.MTdat.TEphaseErr;
ePh = [ ePh ; S.MTdat.TEphase - S.MTdat.TEphaseErr ];

%plot the response:
figure
subplot(2,1,1)
semilogx(1./S.MTdat.freqs,S.MTdat.TEappRes,'or','linewidth',2)
hold on
semilogx([1./S.MTdat.freqs;1./S.MTdat.freqs],eAR,'-k','linewidth',2)

title('Apparent Resistivity ')
ylabel('log_{10}(\rho) (ohm-m)')
set(gca,'fontsize',12)

subplot(2,1,2)
semilogx(1./S.MTdat.freqs,S.MTdat.TEphase,'or','linewidth',2)
hold on
semilogx([1./S.MTdat.freqs;1./S.MTdat.freqs],ePh,'-k','linewidth',2)
ax = axis;
axis([ax(1) ax(2) 0 90]);
xlabel('Period (s)');
ylabel('degrees');
title('Phase')
set(gca,'fontsize',12)

%output the necessary files for Step_2_Run_SERPENT_Inversion
save(FileName,'-struct','S')
save('Trash/WaterColumn.mat','-struct','S1')
save('Trash/Subsurface.mat','-struct','S2')




