% Generates the structure needed to run a trans-dimensional MCMC inversion
% of ocean-bottom CSEM, surface-towed CSEM, and MT data (any or all of the
% above) using PT_RJMCMC.m

clear all
clc

FileNameRoot = 'synthetic_obCSEM';
FileName = ['Trash/',FileNameRoot,'.mat'];

%specify which data types (surface-towed CSEM, ocean-bottom CSEM, MT) to include
ST = false;
OB = true; 
MT = false;
DCR = false;

% this array is used to determine which data types to compute forward
% responses for during the Bayesian inversion
S.dataTypes = false(4,1); 
S.dataTypes(1) = ST;
S.dataTypes(2) = OB;
S.dataTypes(3) = MT;
S.dataTypes(4) = DCR;

% set these to false, since we're doing trans-D MCMC
S.fixedDimension = false;
S.fixedWater = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Transmitter and receiver geometries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% number of receivers
nRx = 20;
% receiver spacing
Rx_spacing = 1e3; % m
% minimum receiver-transmitter distance
minRange = 1e3; % m
% water depth
S.obTx.WaterDepth = 1000; % m
% tow depth for transmitter
towDepth = S.obTx.WaterDepth - 50; % depth above seafloor (m)
% dipole transmitter length
S.obTx.Length = 300;
% receiver x-coordinates (perpendicular to transmitter tow-line)
S.obRx.X = zeros(nRx,1);
% receiver y-coordinates (in-line with transmitter)
S.obRx.Y = linspace(minRange,minRange+Rx_spacing*(nRx-1),nRx)';
% receiver z-coordinates (depth; make sure receivers are in the water)
S.obRx.Z = (S.obTx.WaterDepth - 1)*ones(nRx,1); % place receivers 1 m above seafloor
% Transmitter geometry
S.obTx.X = 0; % m
S.obTx.Y = 0; % m
S.obTx.Z = S.obTx.WaterDepth - towDepth; % m
S.obTx.Azimuth = 90; % degrees
S.obTx.Dip = 0; % degrees
% in 1D CSEM, no point in having more than one transmitter location
S.obTx.Soundings = [ 1 ];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Synthetic model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% resistivity model
x.z = [ S.obTx.WaterDepth , 2e3 , 2e3+500 ];
x.rhoh = [ -0.52 , 1 , 3 , 1 ]; %log10(ohm-m)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MCMC parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%general parameters
S.debug_prior = false; % only sample prior if true. For testing the prior and the forward calls
S.transform01_ab = false; % if you want to use  (0,1) prior, then transform to (a,b)
if( S.transform01_ab == true )
   % load maximum allowed resistivity as a function of depth
end
S.logZ = true;
S.regionBoundaryDepth = S.obTx.WaterDepth;
S.nTemps = 6;         % number of temperatures in PT
nChainsAt1 = 2;       % number of chains at T=1 (increasing this adds more models to the ensemble)
S.nChains = S.nTemps + nChainsAt1; %total number of chains in PT (possibly multiple chains at T=1)
S.Tmax = 2.0;         % Tmax for PT
S.B = [ logspace(-log10(S.Tmax),0,S.nTemps) ones(1,S.nChains-S.nTemps) ]; %inverse temperature ladder
S.numIterations = 1d4;  % 100,000 Number of RJ-MCMC iterations to carry out (should be at least 100,000)
S.saveEvery     = 1d4;   % How often to write the models in the chain to an output file
S.displayEvery  = 1d2;  % How often to display progress to the command window


%This code divides the model space into a water column portion and a
%subsurface portion. This section sets specific parameters for the water column portion of the model
S1.isotropic = true;   % This code is currently isotropic only (this parameter must be 'true')
S1.birth_death_from_prior = true; % proposal distribution = prior for birth/death moves
S1.kInit = 1;          % number of interfaces in the random initial model
S1.kMin = 1 ;          % Minimum number of additional layer interfaces -- 1 is the min allowed here
S1.kMax = 7;           % Maximum number of additional layer interfaces
S1.zMin = 10;   % There is no min thickness, but transmitter and receivers must be in same layer
S1.zMax = towDepth - 1; % Max depth interfaces can be placed at in the water column (should be less than Tx tow depth 
S1.rhoMin = -0.50;     % Minimum log-resistivity of the uniform prior distribution (for water column)
S1.rhoMax = -0.54;     % Maximum log-resistivity of the uniform prior distribution (for water column)
%These step sizes are for the MCMC model space search. Tuning these can be
%a pain, but they only affect rate of convergence, not convergence itself
S1.UstepSize = [ linspace(0.02, 0.002, S.nTemps) 0.002*ones(1,S.nChains-S.nTemps) ]; %update step
S1.BstepSize = [ linspace(0.05, 0.025, S.nTemps) 0.025*ones(1,S.nChains-S.nTemps) ]; %birth step
S1.MstepSize = [ linspace(7, 2, S.nTemps) 2*ones(1,S.nChains-S.nTemps) ]; %move interface step


%This code divides the model space into a water column portion and a
%subsurface portion. This section sets specific parameters for the subsurface portion of the model
S2.isotropic = true;   % This code is currently isotropic only (this parameter must be 'true')
S2.kInit = 1;          % number of interfaces in the random initial model
S2.kMin = 1 ;           % Minimum number of additional layer interfaces
S2.kMax = 40;          % Maximum number of additional layer interfaces
S2.zMin = S.regionBoundaryDepth;   % There is no min thickness, but transmitter and receivers must be in same layer
S2.zMax = 6000; % depth at which to place last interface  
S2.rhoMin = 0;  % Minimum log-resistivity of the uniform prior distribution (for subsurface)
S2.rhoMax = 4;   % Maximum log-resistivity of the uniform prior distribution (for subsurface)
%These step sizes are for the MCMC model space search. Tuning these can be
%a pain, but they only affect rate of convergence, not convergence itself
S2.UstepSize = [ linspace(0.06, 0.01, S.nTemps) 0.01*ones(1,S.nChains-S.nTemps) ]; %for update in log10 rho
S2.BstepSize = [ linspace(0.6, 0.4, S.nTemps) 0.4*ones(1,S.nChains-S.nTemps) ]; %for birth / death in log10 rho
S2.MstepSize = [ linspace(25, 7, S.nTemps) 7*ones(1,S.nChains-S.nTemps) ]; %for move interface in m


if( S.logZ == true )
    x.z = log10(x.z);
    S.regionBoundaryDepth = log10(S.regionBoundaryDepth);
    S1.zMin = log10(S1.zMin);
    S1.zMax = log10(S1.zMax);
    S1.MstepSize = log10(S1.MstepSize);
    S2.zMin = log10(S2.zMin);
    S2.zMax = log10(S2.zMax);
    S2.MstepSize = log10(S2.MstepSize);
end

%beta determines what fraction of the time we do an MCMC step for the water
%column portion of the model. (1-beta): how often we do a subsurface MCMC step
S.beta = 0.2; 

%put in the true model, since this is a synthetic inversion
S.TrueModel.z = [ x.z S2.zMax ];
S.TrueModel.rho = [ x.rhoh x.rhoh(end) ];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate synthetic obCSEM data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% frequencies to invert
S.obDat.Freqs = [ 0.25 ; 0.75 ]; % Hz

% forward response
QQ = get_field_obCSEM(S,x,false);
ErResponse = log10(abs(QQ));
PhaseResponse = (180/pi)*atan2(imag(QQ),real(QQ));

% add noise
%generate random Gaussian deviates and make sure they are reasonable (not out too far in the tails)
u = randn(size(ErResponse));
v = randn(size(PhaseResponse));
w = randn(size(ErResponse));  %to hold the absolute noise for Er
for j=1:3  %do it a few times in a row to make sure that you don't replace one with another
    for k=1:size(ErResponse,1)
        for l=1:size(ErResponse,2)
           if( abs(u(k)) > 2.5 )
               u(k) = randn;
           end
           if( abs(w(k)) > 2.5 )
               w(k) = randn;
           end
           if( abs(v(k)) > 2.5 )
               v(k) = randn;
           end
        end
    end
end
fprintf('Max standard deviation in rel and abs noise: %4.2f and %4.2f\n',max(max(u)),max(max(v)))

%add noise
Er_noiseToAdd = 0.02; % x percent relative error
Phase_noiseToAdd = 0.05; % x percent relative error
noiseFloor = log10(10^(-13)); %noise floor in log10 units
ErNoise = u*Er_noiseToAdd*(1/log(10));% + w*noiseFloor*(1/log(10));
PhaseNoise = v*Phase_noiseToAdd*(180/pi);
Er_data  = ErResponse + ErNoise;
Phase_data = PhaseResponse + PhaseNoise;
Er_sd = Er_noiseToAdd*(1/log(10))*ones(size(Er_data));% + noiseFloor; 
Phase_sd = Phase_noiseToAdd*(180/pi)*ones(size(Phase_data));

% load the synthetic data into the structure
S.obDat.Er = Er_data;
S.obDat.ErErr = Er_sd;
S.obDat.Phase = Phase_data;
S.obDat.PhaseErr = Phase_sd;

% plot the synthetic data
%plot CSEM data
figure(9)
r = sqrt((S.obTx.X - S.obRx.X).^2 + (S.obTx.Y - S.obRx.Y).^2);
r = mean(r,2);
r = r';
for ifreq=1:size(S.obDat.Er,1)
   %make the error bars for plotting
   eER = S.obDat.Er(ifreq,:) + S.obDat.ErErr(ifreq,:);
   eER = [ eER ; S.obDat.Er(ifreq,:) - S.obDat.ErErr(ifreq,:) ];
   ePh = S.obDat.Phase(ifreq,:) + S.obDat.PhaseErr(ifreq,:);
   ePh = [ ePh ; S.obDat.Phase(ifreq,:) - S.obDat.PhaseErr(ifreq,:) ];
   subplot(2,1,1)
   plot(r,S.obDat.Er(ifreq,:),'or','linewidth',2)
   hold on
   plot([r;r],eER,'-k','linewidth',2)
   ylabel('log(|E_r|) (V/Am)')
   set(gca,'fontsize',14)

   subplot(2,1,2)
   plot(r,S.obDat.Phase(ifreq,:),'or','linewidth',2)
   hold on
   plot([r;r],ePh,'-k','linewidth',2)
   ylabel('Phase (^o)')
   xlabel('Source-receiver offset (m)')
   set(gca,'fontsize',14)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save the MCMC input files
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save(FileName,'-struct','S')
save('Trash/WaterColumn.mat','-struct','S1')
save('Trash/Subsurface.mat','-struct','S2')








