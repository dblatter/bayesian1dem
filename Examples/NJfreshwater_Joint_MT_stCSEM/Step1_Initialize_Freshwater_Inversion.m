% Generates the structure needed to run a trans-dimensional MCMC inversion
% of ocean-bottom CSEM, surface-towed CSEM, and MT data (any or all of the
% above) using PT_RJMCMC.m

clear all
clc

%specify which data types (surface-towed CSEM, ocean-bottom CSEM, MT) to include
ST = true;
OB = false; 
MT = true;
DCR = false;

% this array is used to determine which data types to compute forward
% responses for during the Bayesian inversion
S.dataTypes = false(4,1); 
S.dataTypes(1) = ST;
S.dataTypes(2) = OB;
S.dataTypes(3) = MT;
S.dataTypes(4) = DCR;

LineNum = '1'; %the survey line the data comes from

MTnum = 5; %specifies the MT station to invert, if ST == false

%decide which soundings to invert. These are the transmitter numbers in the
%data file (see below)
sBeg = 211;
sEnd = 225;

S.fixedDimension = false;
S.fixedWater = false;

% -------------------------------------------------------------------
% Unless you're modifying the code, you shouldn't have to worry about
% anything below here
% -------------------------------------------------------------------


Soundings = sBeg:sEnd;
S.stTx.Soundings = Soundings;

if( str2num(LineNum) == 1 )
   % load in MT data and related parameters
   load('Examples/NJfreshwater_Joint_MT_stCSEM/Line1_MT_Data.mat')
   
   %load the surface-towed E-field data, receiver locations, transmitter locations
   
   %Dat contains all the electric field amplitudes and phases, and their
   %errors, in log-domain. Each amplitude or phase has a corresponding RxID, TxID,
   %and FreqID. The RxIDs do not correspond to a physical instrument, but to a receiver
   %location at the time of a CSEM sounding (there are Nrx unique RxIDs for
   %each TxID). Dat also contains an array of frequencies. FreqIDs contains
   %the array indices of the corresponding frequencies in Freqs
   Dat = load('Examples/NJfreshwater_Joint_MT_stCSEM/Line1_ST_Data.mat');
   
   %WaterDat contains an array of ocean depths and their corresponding
   %distance from the start of the CSEM tow line
   WaterDat = load('Examples/NJfreshwater_Joint_MT_stCSEM/Line1WaterDepth.mat');
   
   %Rx contains the X,Y,Z, and theta coordinates for each receiver
   %position; there are Nrx receiver positions for each transmitter location,
   %in order (the first Nrx entries are for Tx position 1, etc), where Nrx
   %is the # of receivers in the towed array
   Rx = load('Examples/NJfreshwater_Joint_MT_stCSEM/Line1_ST_RxData.mat');
   
   %Tx contains the azimuth, dip, length, and X,Y,Z coordinates of each
   %transmitter location. All X,Y,Z coordinates are with reference to a
   %coordinate system where Y is the in-line directiion (the direction of
   %the tow line), X is perpendicular to Y, and Z is positive down. The
   %origin is the start point of the tow line.
   Tx = load('Examples/NJfreshwater_Joint_MT_stCSEM/Line1_ST_TxData.mat');
   
elseif( str2num(LineNum) == 6 )
   % load in MT data and related parameters (same story as above, but for a
   % different tow line)
   load('Data/Line6/Line6_MT_Data.mat')
   %load the surface-towed E-field data, receiver locations, transmitter locations
   Dat = load('Data/Line6/Line6_ST_Data.mat');
   WaterDat = load('Data/Line6/Line6WaterDepth.mat');
   Rx = load('Data/Line6/Line6_ST_RxData.mat');
   Tx = load('Data/Line6/Line6_ST_TxData.mat');
else
   display('You have to have data to do inversion you know...')
end

%set this to "true" if you want to average together all the soundings in
%S.stTx.Soundings. The error is correspondingly reduced by sqrt(Ns)
averagingSoundings = true;

% number of surface-towed array receivers
nRx = 4;

%now load into S the relevant data, Rx and Tx locations, etc
for j=1:length(Soundings)
   itx = Soundings(j);
   %transmitter
   S.stTx.X(j) = Tx.X(itx);
   S.stTx.Y(j) = Tx.Y(itx);
   S.stTx.Z(j) = Tx.Z(itx);
   %recievers
   irx = (itx-1)*nRx;
   S.stRx.X(:,j) = Rx.X(irx+1:irx+nRx);
   S.stRx.Y(:,j) = Rx.Y(irx+1:irx+nRx);
   S.stRx.Z(:,j) = Rx.Z(irx+1:irx+nRx);
   %data
   Er = NaN(length(Dat.Freqs),nRx);
   ErErr = Er;
   RxInd = Er;
   Phase = Er;
   PhaseErr = Er;
   for ifreq=1:length(Dat.Freqs)
      k = 0;
      m = 0;
      for l=1:length(Dat.Er)
         if( Dat.ErFreqID(l) == ifreq && Dat.ErTxID(l) == itx )
            k = k + 1;
            Er(ifreq,k) = Dat.Er(l);
            ErErr(ifreq,k) = Dat.ErErr(l);
            RxInd(ifreq,k) = mod(Dat.ErRxID(l),nRx);
            %keyboard
         end
      end
      for l=1:length(Dat.Phase)
         if( Dat.PhaseFreqID(l) == ifreq && Dat.PhaseTxID(l) == itx )
            m = m + 1;
            Phase(ifreq,m) = Dat.Phase(l);
            PhaseErr(ifreq,m) = Dat.PhaseErr(l);
         end
      end
   end
   RxInd(RxInd == 0) = nRx; %mod(4,4) == 0, but we want the 4th receiver to be labeled 4, not 0
   S.stDat.Er(:,:,j) = Er;
   S.stDat.ErErr(:,:,j) = ErErr;
   S.stDat.Phase(:,:,j) = Phase;
   S.stDat.PhaseErr(:,:,j) = PhaseErr;
end

%this portion of the code is to average multiple soundings to reduce data
%noise. It assumes that all the soundings you have loaded so far are to be
%averaged together.
if( averagingSoundings == true )
   
   Z = 10.^(S.stDat.Er).*exp(1i*(S.stDat.Phase*pi/180));  %convert back to complex fields again
   
   meanZ = nanmean(Z,3);  %average of the complex fields -- must average THESE, not |E|'s
   Er_notlog = abs(meanZ);  %|E| is the magnitude of the average of the complex fields
   S.stDat.Er = log10(Er_notlog);  %convert to log-domain -- this is our averaged Er data
   
   %phase is the phase of the average of the complex fields
   S.stDat.Phase = atan2(imag(meanZ),real(meanZ))*180/pi;
   
   %The variance in Z
   Z_var = sqrt((var(real(Z),0,3).^2 + var(imag(Z),0,3).^2))/sqrt(2);
   %Variance in meanZ is simply variance in Z, reduced by N
   meanZ_var = Z_var/size(Z,3);
   %Standard deviation of the mean of Z
   meanZ_std = sqrt(meanZ_var);

   %The |E| error in log-domain is a function of the relative error
   Er_relerr = meanZ_std ./ Er_notlog;
   % Apply 1% error floor:
   err_floor = 0.01; 
   Er_relerr(Er_relerr<err_floor) = err_floor;
   %The error in the log of |E| is the relative error / ln(10)
   S.stDat.ErErr = Er_relerr/log(10);
   
   %The error in phase is the relateive error in |E| (in rad), converted back into degrees
   S.stDat.PhaseErr = Er_relerr*180/pi;
   
   %randomly choose one sounding so that the soundings vector has length 1
   S.stTx.Soundings = Soundings(randi(length(Soundings)));
   
   %take the average of the receiver locations
   S.stRx.X = mean(S.stRx.X,2);
   S.stRx.Y = mean(S.stRx.Y,2);
   S.stRx.Z = mean(S.stRx.Z,2);
   
   %take the average of the transmitter locations
   S.stTx.X = mean(S.stTx.X,2);
   S.stTx.Y = mean(S.stTx.Y,2);
   S.stTx.Z = mean(S.stTx.Z,2);
  
end

S.stTx.Dip = Tx.Dip;
S.stTx.Length = Tx.Length;
S.stTx.Azimuth = Tx.Azimuth;
%add in the frequencies used
S.stDat.Freqs = Dat.Freqs;


%--------------------------------------------------------------------------------
% The final data to be inverted is in place. The CSEM data is in the form of arrays
% of size (Nfreqs x Nrx x NTx ) -- one for Er, Er error, Phase, Phase
% error. If you chose to average several Txs, the last dimension is
% omitted. There is also a CSEM frequency array of size (Nfreqs). The MT
% data contains 4 arrays as well of size (Nfreqsmt) -- apparent
% resistivity, its error, phase, and its error. Also a freqs array of the
% same size
%---------------------------------------------------------------------------------


%extract ocean depth values for each transmitter location
TxR = Tx.Y*1d-3; %convert to km
l = find(WaterDat.Range > 0,1);
q = find(WaterDat.Range < TxR(end),1,'last');
WaterDat.Range = WaterDat.Range(l:q);
WaterZ = WaterDat.Depth(l:q);
%now interpolate to the transmitter locations
for j=1:length(Soundings)
   itx = Soundings(j);
   for k=1:length(WaterDat.Range)
      if( TxR(itx) < WaterDat.Range(k) && k > 1 )
         r1 = TxR(itx) - WaterDat.Range(k-1);
         r2 = WaterDat.Range(k) - TxR(itx);
         Tx.WaterDepth(j) = - (WaterZ(k-1)*r2 + WaterZ(k)*r1) / (r1 + r2);
         break
      elseif( TxR(itx) < WaterDat.Range(k) && k == 1 )
         Tx.WaterDepth(j) = - WaterZ(k);
         break
      end
   end
end

S.stTx.WaterDepth = mean(Tx.WaterDepth);
if( MT == true && ST == false ) %the MT doesn't care about the water depth, this is just for plotting purposes
   S.stTx.WaterDepth = MTwaterDepth(MTnum);
end

if( ST == true )
   %find the MT station nearest the transmitter location(s) being inverted
   [~,ind] = min(abs( MTrange - mean(S.stTx.Y) ));
elseif( ST == false && MT == true )
   ind = MTnum;
else
   display('Are you really inverting anything?')
   keyboard
end

%We don't want to use MT data longer than 10s period, due to wave noise and
%the fact that we're interested in shallow structure (towed CSEM system is
%only sensitive to upper km or so)
PeriodCut = 10;
ind2 = find(1./MTfreqs{ind} < PeriodCut,1,'last');

%pick out only those frequencies that are shorter than PeriodCut (in sec)
S.MTdat.freqs = MTfreqs{ind}(1:ind2); %allfreqs; 
S.MTdat.TEappRes = TEappRes{ind}(1:ind2); %allAppRes; 
S.MTdat.TEappResErr = TEappResErr{ind}(1:ind2); %allAppResErr;
S.MTdat.TEphase = TEphase{ind}(1:ind2); %allPhase; 
S.MTdat.TEphaseErr = TEphaseErr{ind}(1:ind2); %allPhaseErr; 


%RJMCMC parameters:

%general parameters
S.debug_prior = false; % only sample prior if true. For testing the prior and the forward calls
S.transform01_ab = false; % if you want to use  (0,1) prior, then transform to (a,b)
if( S.transform01_ab == true )
   % load maximum allowed resistivity as a function of depth

end
S.logZ = false;
S.nTemps = 6;         % number of temperatures in PT
S.regionBoundaryDepth = S.stTx.WaterDepth;
nChainsAt1 = 2;       % number of chains at T=1 (increasing this adds more models to the ensemble)
S.nChains = S.nTemps + nChainsAt1; %total number of chains in PT (possibly multiple chains at T=1)
S.Tmax = 2.0;         % Tmax for PT
S.B = [ logspace(-log10(S.Tmax),0,S.nTemps) ones(1,S.nChains-S.nTemps) ]; %inverse temperature ladder
S.numIterations = 2d4;  % 100,000 Number of RJ-MCMC iterations to carry out (should be at least 100,000)
S.saveEvery     = 2d5;   % How often to write the models in the chain to an output file
S.displayEvery  = 1d3;  % How often to display progress to the command window


%This code divides the model space into a water column portion and a
%subsurface portion. This section sets specific parameters for the water column portion of the model
S1.isotropic = true;   % This code is currently isotropic only (this parameter must be 'true')
S1.birth_death_from_prior = true; % proposal distribution = prior for birth/death moves
S1.kInit = 1;          % number of interfaces in the random initial model
S1.kMin = 1 ;          % Minimum number of additional layer interfaces -- 1 is the min allowed here
S1.kMax = 7;           % Maximum number of additional layer interfaces
S1.zMin = max([max(S.stTx.Z) ; max(max(S.stRx.Z))]) + 0.1;   % There is no min thickness, but transmitter and receivers must be in same layer
S1.zMax = S.regionBoundaryDepth; % An interface is automatically placed at zMax, and should be the water depth 
S1.rhoMin = -0.75;     % Minimum log-resistivity of the uniform prior distribution (for water column)
S1.rhoMax = -0.55;     % Maximum log-resistivity of the uniform prior distribution (for water column)
%These step sizes are for the MCMC model space search. Tuning these can be
%a pain, but they only affect rate of convergence, not convergence itself
S1.UstepSize = [ linspace(0.02, 0.002, S.nTemps) 0.002*ones(1,S.nChains-S.nTemps) ]; %update step
S1.BstepSize = [ linspace(0.05, 0.025, S.nTemps) 0.025*ones(1,S.nChains-S.nTemps) ]; %birth step
S1.MstepSize = [ linspace(7, 2, S.nTemps) 2*ones(1,S.nChains-S.nTemps) ]; %move interface step

%This code divides the model space into a water column portion and a
%subsurface portion. This section sets specific parameters for the subsurface portion of the model
S2.isotropic = true;   % This code is currently isotropic only (this parameter must be 'true')
S2.kInit = 1;          % number of interfaces in the random initial model
S2.kMin = 1 ;           % Minimum number of additional layer interfaces
S2.kMax = 100;          % Maximum number of additional layer interfaces
S2.zMin = S.regionBoundaryDepth;   % There is no min thickness, but transmitter and receivers must be in same layer
S2.zMax = 2000; % depth at which to place last interface  
S2.rhoMin = -1;  % Minimum log-resistivity of the uniform prior distribution (for subsurface)
S2.rhoMax = 3;   % Maximum log-resistivity of the uniform prior distribution (for subsurface)
%These step sizes are for the MCMC model space search. Tuning these can be
%a pain, but they only affect rate of convergence, not convergence itself
S2.UstepSize = [ linspace(0.06, 0.01, S.nTemps) 0.0175*ones(1,S.nChains-S.nTemps) ]; %for update in log10 rho
S2.BstepSize = [ linspace(0.6, 0.4, S.nTemps) 0.4*ones(1,S.nChains-S.nTemps) ]; %for birth / death in log10 rho
S2.MstepSize = [ linspace(50, 7, S.nTemps) 15*ones(1,S.nChains-S.nTemps) ]; %for move interface in m

if( S.logZ == true )
    S.regionBoundaryDepth = log10(S.regionBoundaryDepth);
    S1.zMin = log10(S1.zMin);
    S1.zMax = log10(S1.zMax);
    S1.MstepSize = log10(S1.MstepSize);
    S2.zMin = log10(S2.zMin);
    S2.zMax = log10(S2.zMax);
    S2.MstepSize = log10(S2.MstepSize);
end

%beta determines what fraction of the time we do an MCMC step for the water
%column portion of the model. (1-beta): how often we do a subsurface MCMC step
S.beta = 0.0; 

%This determines the output file name (using the choice of data inverted)
%and writes the necessary output files. WaterColumn.mat and Subsurface.mat
%contain the parameters unique to these portions of the model. The other .mat
%file contains all the general parameters, as well as the data
FilePrefix = 'Trash/Line'; %1_Joint_ST';
if( ST == true && MT == true )
   invtype = 'Joint';
   FileName = [ FilePrefix LineNum '_' invtype '_ST' num2str(sBeg) '-' num2str(sEnd) '_MT' num2str(ind) '.mat' ];
elseif( ST == true && MT == false)
   invtype = 'STonly';
   FileName = [ FilePrefix LineNum '_' invtype '_ST' num2str(sBeg) '-' num2str(sEnd) '.mat' ];
elseif( ST == false && MT == true )
   invtype = 'MTonly';
   FileName = [ FilePrefix LineNum '_' invtype '_MT' num2str(ind) '.mat' ];
elseif( ST == false && MT == false )
   display('you gotta invert something!!')
   return
end
%FileName = [ FilePrefix LineNum invtype '_ST' num2str(sBeg) '-' num2str(sEnd) '_MT' num2str(ind) '.mat' ];
save(FileName,'-struct','S')
save('Trash/WaterColumn.mat','-struct','S1')
save('Trash/Subsurface.mat','-struct','S2')


%This portion of the code plots the MT and CSEM data you chose to invert

%for plotting error bars
eAR = S.MTdat.TEappRes + S.MTdat.TEappResErr;
eAR = [ eAR ; S.MTdat.TEappRes - S.MTdat.TEappResErr ];
ePh = S.MTdat.TEphase + S.MTdat.TEphaseErr;
ePh = [ ePh ; S.MTdat.TEphase - S.MTdat.TEphaseErr ];

% Plot the response:
figure
subplot(2,1,1)
semilogx(1./S.MTdat.freqs,S.MTdat.TEappRes,'or','linewidth',2)
hold on
semilogx([1./S.MTdat.freqs;1./S.MTdat.freqs],eAR,'-k','linewidth',2)

title('Apparent Resistivity ')
ylabel('log_{10}(\rho) (ohm-m)')
set(gca,'fontsize',12)

subplot(2,1,2)
semilogx(1./S.MTdat.freqs,S.MTdat.TEphase,'or','linewidth',2)
hold on
semilogx([1./S.MTdat.freqs;1./S.MTdat.freqs],ePh,'-k','linewidth',2)
ax = axis;
% axis([ax(1) ax(2) 0 90]);
xlabel('Period (s)');
ylabel('degrees');
title('Phase')
set(gca,'fontsize',12)


%plot CSEM data
figure(9)
r = sqrt((S.stTx.X - S.stRx.X).^2 + (S.stTx.Y - S.stRx.Y).^2);
r = mean(r,2);
r = r';
for ifreq=1:size(S.stDat.Er,1)
   %make the error bars for plotting
   eER = S.stDat.Er(ifreq,:) + S.stDat.ErErr(ifreq,:);
   eER = [ eER ; S.stDat.Er(ifreq,:) - S.stDat.ErErr(ifreq,:) ];
   ePh = S.stDat.Phase(ifreq,:) + S.stDat.PhaseErr(ifreq,:);
   ePh = [ ePh ; S.stDat.Phase(ifreq,:) - S.stDat.PhaseErr(ifreq,:) ];
   subplot(2,1,1)
   plot(r,S.stDat.Er(ifreq,:),'or','linewidth',2)
   hold on
   plot([r;r],eER,'-k','linewidth',2)
   ylabel('log(|E_r|) (V/Am)')
   set(gca,'fontsize',14)

   subplot(2,1,2)
   plot(r,S.stDat.Phase(ifreq,:),'or','linewidth',2)
   hold on
   plot([r;r],ePh,'-k','linewidth',2)
   ylabel('Phase (^o)')
   xlabel('Source-receiver offset (m)')
   set(gca,'fontsize',14)
end





