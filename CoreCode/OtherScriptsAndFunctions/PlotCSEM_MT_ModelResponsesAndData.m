function [] = PlotCSEM_MT_ModelResponsesAndData(FileName,MT,DCR,stCSEM,obCSEM)

%load in the model ensemble
FileName2 = [ FileName '_PT_RJMCMC_Combined.mat' ];
U = load(FileName2);
%load in the data and meta-data
S = load(FileName);
%find out how far we got down the Markov chain
iComputed = find(U.k2_ll,1,'last');
NtoCalc = 50;  %we're gonna use this many samples to calculate r.m.s. at each frequency
NtoPlot = 50; % we're gonna plot this many models from the ensemble
skip = ceil(NtoCalc/NtoPlot);
rng('shuffle') %let's make sure it's a different random sample each time
%initialize arrays to hold model responses
if( MT == 1 )
   ModEnsAppRes = zeros(NtoCalc,length(S.MTdat.freqs));
   ModEnsPhase = zeros(NtoCalc,length(S.MTdat.freqs));
end
if( DCR == 1 )
   ModEnsDCRappRes = zeros(NtoCalc,length(S.dcrDat.es));
end
if( stCSEM == 1 )
   ModEnsEr = zeros(length(S.stDat.Freqs),size(S.stRx.X,1),NtoCalc);
   ModEnsCSEMphase = ModEnsEr;
end
if( obCSEM == 1 )
   ModEnsobEr = zeros(length(S.obDat.Freqs),size(S.obRx.X,1),NtoCalc);
   ModEnsobCSEMphase = ModEnsobEr;
end

if( S.logZ == true )
   S.regionBoundaryDepth = 10^(S.regionBoundaryDepth);
   if( S.transform01_ab == true )
      S.zMin = 10^(S.zMin);
      S.zMax = 10^(S.zMax);
      % re-zero to the seafloor for MT
      S_MT = S;
      S_MT.zMin = S_MT.zMin - S_MT.Tx.WaterDepth;
      S_MT.zMax = S_MT.zMax - S_MT.Tx.WaterDepth;
   end
else
   if( S.transform01_ab == true )
      % re-zero to the seafloor for MT
      S_MT = S;
      S_MT.zMin = S_MT.zMin - S_MT.Tx.WaterDepth;
      S_MT.zMax = S_MT.zMax - S_MT.Tx.WaterDepth;
   end
end

%randomly select NtoPlot models and compute their responses
for l=1:NtoCalc
    indexes(l) = ceil(iComputed*rand);
    if( MT == true )
       %MT first
       rho = U.s_ll{indexes(l)}.rhoh;
       z = U.s_ll{indexes(l)}.z;
       if( S.logZ == true )
          z = 10.^(z);
       end
       % find where the subsurface starts
       pos = find(z == S.regionBoundaryDepth,1,'first');
       % re-zero to the seafloor, and grab only the subsurface part
       z = z(pos+1:end) - S.regionBoundaryDepth;
       rho = rho(pos+1:end);
       if( S.transform01_ab == true ) % transform from (0,1) -> (a,b)
          x.z = z;
          x.rho = rho;
          if( S.logZ == false )
            [rho,z] = Transform01_ab(x,S_MT);
          else
            [rho,z] = Transform01_ab(x,S_MT);
          end
          rho = [rho rho(end)];
       end
       z = [0 z];
       h = z(2:end) - z(1:end-1);
       rho = 10.^(rho);
       [AppRes, Phase, Z] = MT1D(rho,h,S.MTdat.freqs);
       AppRes = log10(AppRes);
       ModEnsAppRes(l,:) = AppRes;
       ModEnsPhase(l,:) = Phase;

       % compute separate RMS's for phase and apparent resistivity
       for q=1:NtoPlot
          MTphaseRMS(q) = sqrt(sum(((ModEnsPhase(q,:) - S.MTdat.TEphase)./S.MTdat.TEphaseErr).^2)/length(S.MTdat.TEphase));
          MTappResRMS(q) = sqrt(sum(((ModEnsAppRes(q,:) - S.MTdat.TEappRes)./S.MTdat.TEappResErr).^2)/length(S.MTdat.TEappRes));
       end

    end
    % next is DCR
    if( DCR == true )
       % grab rho and z --- the model
       rho = U.s_ll{indexes(l)}.rhoh;
       z = U.s_ll{indexes(l)}.z;
       if( S.logZ == true ) % convert back to linear depth if necessary
          z = 10.^(z);
       end
       % find where the subsurface starts
       pos = find(z == S.regionBoundaryDepth,1,'first');
       % re-zero to the seafloor, and grab only the subsurface part
       z = z(pos+1:end) - S.regionBoundaryDepth;
       rho = rho(pos+1:end);
       % compute layer thicknesses
       z = [0 z];
       h = z(2:end) - z(1:end-1);
       DCRappRes = SFilt(S.dcrDat.es, h, rho)';
       ModEnsDCRappRes(l,:) = DCRappRes;
    end
    %Now surface-towed CSEM
    if( stCSEM == true )
       Tx = [ mean(S.stTx.X) mean(S.stTx.Y) mean(S.stTx.Z) S.stTx.Azimuth S.stTx.Dip ];
       Rx = [ mean(S.stRx.X,2) mean(S.stRx.Y,2) mean(S.stRx.Z,2) ];
       rho = U.s_ll{indexes(l)}.rhoh;
       z = U.s_ll{indexes(l)}.z;
       if( S.logZ == true )
          z = 10.^(z);
       end
       z = [ -100d3 ; 0.0 ; z' ]; %format the interface depths and resistivities to stCSEM forward code specs
       rho = [ 1d12 ; 10.^(rho)' ];
       trueModel = [ z rho ];
       for k=1:length(S.stDat.Freqs)
          allFields = Dipole1D(Tx,S.stDat.Freqs(k),trueModel,Rx,0,0,S.stTx.Length,3);
          Er(k,:) = allFields(:,5); %column 5 is the inline E-field
       end
       ModEnsEr(:,:,l) = log10(abs(Er));
       ModEnsCSEMphase(:,:,l) = (180/pi)*atan2(imag(Er),real(Er));
    end
    %Now ocean-bottom CSEM
    if( obCSEM == true )
       Tx = [ S.obTx.X S.obTx.Y S.obTx.Z S.obTx.Azimuth S.obTx.Dip ];
       Rx = [ S.obRx.X, S.obRx.Y S.obRx.Z ];
       rho = U.s_ll{indexes(l)}.rhoh;
       z = U.s_ll{indexes(l)}.z;
       if( S.logZ == true )
          z = 10.^(z);
       end
       z = [ -100d3 ; 0.0 ; z' ]; %format the interface depths and resistivities to obCSEM forward code specs
       rho = [ 1d12 ; 10.^(rho)' ];
       x = [ z rho ];
       for k=1:length(S.obDat.Freqs)
          allFields = Dipole1D(Tx,S.obDat.Freqs(k),x,Rx,0,0,0,3);
          Er(k,:) = allFields(:,5); %column 5 is the inline E-field
       end
       ModEnsobEr(:,:,l) = log10(abs(Er));
       ModEnsobCSEMphase(:,:,l) = (180/pi)*atan2(imag(Er),real(Er));
    end
end

%Added ability to plot misfit as a function of frequency for sample of models (only for MT for now)
if (MT == true)
    % Calculate the r.m.s. for each apparent resistivity and phase as a
    % function of frequency over a sub-sample of models
    rms_appres = nan(NtoCalc,1);
    rms_phase = nan(NtoCalc,1);
    rms_tot = nan(NtoCalc,1);
    for i = 1:NtoCalc

        appResRes = ((S.MTdat.TEappRes - ModEnsAppRes(i,:))./S.MTdat.TEappResErr);
        phaseRes = ((S.MTdat.TEphase - ModEnsPhase(i,:))./S.MTdat.TEphaseErr);

        rms_appres(i) = sqrt((1/length(appResRes))*sum(appResRes.^2));
        rms_phase(i) = sqrt((1/length(phaseRes))*sum(phaseRes.^2));

        rms_tot(i) = sqrt((1/(length(appResRes)+length(phaseRes))*sum(appResRes.^2 + phaseRes.^2)));

    end

    f_rms_appres = nan(length(AppRes),1);
    f_rms_phase = nan(size(f_rms_appres));
    f_rms_tot = nan(size(f_rms_appres));
    for i = 1:length(AppRes)

        appResRes = ((S.MTdat.TEappRes(i) - ModEnsAppRes(:,i))./S.MTdat.TEappResErr(i));
        phaseRes = ((S.MTdat.TEphase(i) - ModEnsPhase(:,i))./S.MTdat.TEphaseErr(i));

        f_rms_appres(i) = sqrt((1/length(appResRes))*sum(appResRes.^2));
        f_rms_phase(i) = sqrt((1/length(phaseRes))*sum(phaseRes.^2));

        f_rms_tot(i) = sqrt((1/(length(appResRes)+length(phaseRes))*sum(appResRes.^2 + phaseRes.^2)));

    end

    figure(1);
    semilogx(1./S.MTdat.freqs,f_rms_tot,'-k','LineWidth',2); hold on

    semilogx(1./S.MTdat.freqs,f_rms_appres,'--b');
    semilogx(1./S.MTdat.freqs,f_rms_phase,'--g');
    xlabel('Period (s)');
    ylabel('r.m.s.')

    legend('Total r.m.s.','\rho_a r.m.s.','\phi r.m.s.')
end


%now that we have the data and randomly selected model responses, let's plot them

if( MT == true )

   %for plotting error bars
   eAR = S.MTdat.TEappRes + S.MTdat.TEappResErr;
   eAR = [ eAR ; S.MTdat.TEappRes - S.MTdat.TEappResErr ];
   ePh = S.MTdat.TEphase + S.MTdat.TEphaseErr;
   ePh = [ ePh ; S.MTdat.TEphase - S.MTdat.TEphaseErr ];

   figure
   subplot(1,2,2)
   %plot the model responses in gray
   for l=1:skip:NtoCalc
   semilogx(1./S.MTdat.freqs,ModEnsAppRes(l,:),'-*','Color',[0.8 0.8 0.8])
   hold on
   end
   %plot the real data in red with error bars in black
   semilogx(1./S.MTdat.freqs,S.MTdat.TEappRes,'or','linewidth',2)
   semilogx([1./S.MTdat.freqs;1./S.MTdat.freqs],eAR,'-k','linewidth',2)
   %title('Apparent Resistivity ')
   xlabel('Period (s)');
   ylabel('log_{10}(\rho) (ohm-m)')
   set(gca,'fontsize',14)

   subplot(1,2,1)
   for l=1:skip:NtoCalc
   semilogx(1./S.MTdat.freqs,ModEnsPhase(l,:),'-*','Color',[0.8 0.8 0.8])
   hold on
   end
   semilogx(1./S.MTdat.freqs,S.MTdat.TEphase,'or','linewidth',2)
   semilogx([1./S.MTdat.freqs;1./S.MTdat.freqs],ePh,'-k','linewidth',2)
   ax = axis;
   % axis([ax(1) ax(2) 0 90]);
   xlabel('Period (s)');
   ylabel('degrees');
   %title('Phase')
   set(gca,'fontsize',14)
   pos = [341   538   987   398];
   set(gcf,'position',pos)

   figure
   histogram(MTphaseRMS,'normalization','pdf')
   hold on
   histogram(MTappResRMS,'normalization','pdf')
   ylabel('PDF')
   xlabel('RMS misfit')
   legend('phase','apparent resistivity')


end


if( DCR == true )
   figure
   for l=1:skip:NtoCalc
   plot(S.dcrDat.es,ModEnsDCRappRes(l,:),'-*','Color',[0.8 0.8 0.8])
   hold on
   end
   errorbar(S.dcrDat.es, S.dcrDat.appRes, 2*S.dcrDat.appResErr, 'ro');
   xlabel('electrode spacing (m)')
   ylabel('log(apparent resistivity) (ohm-m)')
   set(gca,'XScale','log')
   set(gca,'FontSize',14)
end


if( stCSEM == true )

   figure
   r = sqrt((S.stTx.X - S.stRx.X).^2 + (S.stTx.Y - S.stRx.Y).^2);
   r = mean(r,2);
   r = r';
   %plot the model ensemble responses first
   for l=1:skip:NtoCalc
      for ifreq=1:length(S.stDat.Freqs)
         subplot(2,1,1)
         plot(r,ModEnsEr(ifreq,:,l),'-*','Color',[0.8 0.8 0.8])
         hold on
         subplot(2,1,2)
         plot(r,ModEnsCSEMphase(ifreq,:,l),'-*','Color',[0.8 0.8 0.8])
         hold on
      end
   end
   %now plot the CSEM data
   for ifreq=1:size(S.stDat.Er,1)
      %make the error bars for plotting
      eER = S.stDat.Er(ifreq,:) + S.stDat.ErErr(ifreq,:);
      eER = [ eER ; S.stDat.Er(ifreq,:) - S.stDat.ErErr(ifreq,:) ];
      ePh = S.stDat.Phase(ifreq,:) + S.stDat.PhaseErr(ifreq,:);
      ePh = [ ePh ; S.stDat.Phase(ifreq,:) - S.stDat.PhaseErr(ifreq,:) ];
      subplot(2,1,1)
      plot(r,S.stDat.Er(ifreq,:),'or','linewidth',2)
      hold on
      plot([r;r],eER,'-k','linewidth',2)
      ylabel('log(|E_r|) (V/Am)')
      set(gca,'fontsize',14)

      subplot(2,1,2)
      plot(r,S.stDat.Phase(ifreq,:),'or','linewidth',2)
      hold on
      plot([r;r],ePh,'-k','linewidth',2)
      ylabel('Phase (^o)')
      xlabel('Source-receiver offset (m)')
      set(gca,'fontsize',14)
   end

end

if( obCSEM == true )

   figure
   r = sqrt((S.obTx.X - S.obRx.X).^2 + (S.obTx.Y - S.obRx.Y).^2);
   %r = mean(r,2);
   r = r';
   %plot the model ensemble responses first
   for l=1:skip:NtoCalc
      for ifreq=1:length(S.obDat.Freqs)
         subplot(2,1,1)
         plot(r,ModEnsobEr(ifreq,:,l),'-*','Color',[0.8 0.8 0.8])
         hold on
         subplot(2,1,2)
         plot(r,ModEnsobCSEMphase(ifreq,:,l),'-*','Color',[0.8 0.8 0.8])
         hold on
      end
   end
   %now plot the CSEM data
   for ifreq=1:size(S.obDat.Er,1)
      %make the error bars for plotting
      eER = S.obDat.Er(ifreq,:) + S.obDat.ErErr(ifreq,:);
      eER = [ eER ; S.obDat.Er(ifreq,:) - S.obDat.ErErr(ifreq,:) ];
      ePh = S.obDat.Phase(ifreq,:) + S.obDat.PhaseErr(ifreq,:);
      ePh = [ ePh ; S.obDat.Phase(ifreq,:) - S.obDat.PhaseErr(ifreq,:) ];
      subplot(2,1,1)
      plot(r,S.obDat.Er(ifreq,:),'or','linewidth',2)
      hold on
      plot([r;r],eER,'-k','linewidth',2)
      ylabel('log(|E_r|) (V/Am)')
      set(gca,'fontsize',14)

      subplot(2,1,2)
      plot(r,S.obDat.Phase(ifreq,:),'or','linewidth',2)
      hold on
      plot([r;r],ePh,'-k','linewidth',2)
      ylabel('Phase (^o)')
      xlabel('Source-receiver offset (m)')
      set(gca,'fontsize',14)
   end

end

end
