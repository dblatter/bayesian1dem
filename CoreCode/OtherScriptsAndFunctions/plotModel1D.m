function [] = plotModel1D(x)% plotModel1D(b,l,x)

   z = [ 0 x.z ];
   if( isfield(x,'rhoh') == 1 )
      r = x.rhoh;
   elseif( isfield(x,'rho') == 1 )
      r = x.rho;
   else
      display('you need a resistivity profile')
   end

for i=1:length(r)
   k = 2*i-1;
   rr(k) = r(i);
   rr(k+1) = r(i);
end
for i=1:length(z)-1
    k = 2*i - 1;
   zz(k) = z(i);
   zz(k+1) = z(i+1);
end
zz(k+2) = z(end);
zz(k+3) = 1.1*z(end);

plot(rr,zz,'--c','linewidth',2)
set (gca,'ydir','reverse')
hold on


end