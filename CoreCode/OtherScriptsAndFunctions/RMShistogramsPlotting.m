
% this code inputs a CSEM-only, an MT-only, and a joint model ensemble and
% outputs two histograms of RMS misfit -- one comparing the MT-only to the
% MT misfit of the joint inversion ensemble; and the other doing the same
% for the CSEM data

clear all
clc
close all

%load in the model ensembles
fprintf('loading model ensembles...\n')
MT = load('Trash/Line1_MTonly_MT5_PT_RJMCMC_8.mat');
fprintf('MT ensemble loaded\n')
CSEM = load('Trash/Line1_STonly_ST196-210_PT_RJMCMC_8.mat');
fprintf('CSEM ensemble loaded\n')
Joint = load('Trash/Line1_Joint_ST196-210_MT5_PT_RJMCMC_8.mat');
fprintf('Joint ensemble loaded\n')

%burn-in
burnIn = 3d5;

color2 = [ 0.8500    0.3250    0.0980 ];
color1 = [ 0.4660    0.6740    0.1880 ];
color3 = [ 0         0.4470    0.7410 ];

figure(1)
subplot(1,3,1)
H = histogram(MT.indivRMSs_ll(burnIn:end,1),'facecolor',color1,'edgecolor','none');
hold on
histogram(Joint.indivRMSs_ll(burnIn:end,2),H.BinEdges,'facecolor',color3,'edgecolor','none');
%xlim([0.4 1.7])
xlabel('RMS misfit')
ylabel('Bin count')
legend('MT-only','Joint')
set(gca,'fontsize',14)

subplot(1,3,2)
H = histogram(CSEM.indivRMSs_ll(burnIn:end,1),'facecolor',color2,'edgecolor','none');
hold on
histogram(Joint.indivRMSs_ll(burnIn:end,1),H.BinEdges,'facecolor',color3,'edgecolor','none');
%xlim([0.4 1.7])
xlabel('RMS misfit')
ylabel('Bin count')
legend('CSEM-only','Joint')
set(gca,'fontsize',14)

subplot(1,3,3)
H = histogram(Joint.en_ll(burnIn:end,2),'facecolor',color3,'edgecolor','none');
%xlim([0.4 1.7])
xlabel('RMS misfit')
ylabel('Bin count')
legend('Joint')
set(gca,'fontsize',14)

pos = [ 1596         981        1249         499 ];
set(gcf,'Position',pos)

%% Pick out only those models that fit both data sets to within the median of both

medMT = median(Joint.indivRMSs_ll(burnIn:end,2));
medCSEM = median(Joint.indivRMSs_ll(burnIn:end,1));

N = length(Joint.indivRMSs_ll(burnIn:end,1));
k = 1;
for j=burnIn:N
   if( Joint.indivRMSs_ll(j,1) < medCSEM && Joint.indivRMSs_ll(j,2) < medMT )
      GoodFitInds(k) = j;
      k = k + 1;
   end
end

s_ll = cell(length(GoodFitInds),1);
k1_ll = zeros(length(GoodFitInds),1);
k2_ll = zeros(length(GoodFitInds),1);
for j=1:length(GoodFitInds)
   s_ll{j} = Joint.s_ll{GoodFitInds(j)};
   k1_ll(j) = Joint.k1_ll(GoodFitInds(j));
   k2_ll(j) = Joint.k2_ll(GoodFitInds(j));
end

save('Trash/Line1_Joint_ST196-210_MT5_PT_RJMCMC_GoodFits.mat','s_ll','k1_ll','k2_ll')

%% plot the RMS misfit histograms of the good fit models against the total ensemble

MTgf = Joint.indivRMSs_ll(GoodFitInds,2);
CSEMgf = Joint.indivRMSs_ll(GoodFitInds,1);

figure(4)
subplot(2,2,1)
H = histogram(Joint.indivRMSs_ll(burnIn:end,2),'edgecolor','none');
hold on
histogram(MTgf,H.BinEdges,'edgecolor','none')
ylabel('Bin count')
legend('all MT','both-fitting')
set(gca,'fontsize',14)
ax1 = gca;

subplot(2,2,3)
H = histogram(Joint.indivRMSs_ll(burnIn:end,1),'edgecolor','none');
hold on
histogram(CSEMgf,H.BinEdges,'edgecolor','none')
ylabel('Bin count')
xlabel('RMS misfit')
legend('all CSEM','both-fitting')
set(gca,'fontsize',14)
ax2 = gca;

x1 = min([ ax1.XLim(1) ax2.XLim(1) ]);
x2 = max([ ax1.XLim(2) ax2.XLim(2) ]);
xlim([x1 x2])
subplot(2,2,1)
xlim([x1 x2])



CSEMxMT = Joint.indivRMSs_ll(burnIn:end,1) .* Joint.indivRMSs_ll(burnIn:end,2);
CSEMxMTgf = Joint.indivRMSs_ll(GoodFitInds,1) .* Joint.indivRMSs_ll(GoodFitInds,2);

subplot(2,2,2)
H = histogram(CSEMxMT,'edgecolor','none');
hold on
histogram(CSEMxMTgf,H.BinEdges,'edgecolor','none')
xlabel('(MT RMS misfit) x (CSEM RMS misfit)')
ylabel('Bin count')
legend('all models','both-fitting')
set(gca,'fontsize',14)

subplot(2,2,4)
scatter(Joint.indivRMSs_ll(burnIn:2:end,1),Joint.indivRMSs_ll(burnIn:2:end,2),...
   5,'marker','.','MarkerFaceAlpha',0.05,'MarkerEdgeAlpha',0.05)
hold on
scatter(Joint.indivRMSs_ll(GoodFitInds,1),Joint.indivRMSs_ll(GoodFitInds,2),...
   10,'marker','.','MarkerFaceAlpha',0.03,'MarkerEdgeAlpha',0.03)
set(gca,'fontsize',14)
xlabel('RMS misfit (CSEM data)')
ylabel('RMS misfit (MT data)')

set(gcf,'position',pos)








