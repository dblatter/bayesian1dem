
function [totalRMS] = CombineChains(FileName,burnIn,nthin,nChains,nChainsAtOne)

   s_ll = [];
   k1_ll = [];
   k2_ll = [];
   totalRMS = [];
   
   for k=1:nChainsAtOne
      fprintf('Processing chain %d out of %d\n',k,nChainsAtOne)
      chainInd = nChains - nChainsAtOne + k;
      U = load([FileName '_' num2str(chainInd) '.mat']);
      j = find(U.k2_ll,1,'last');
      totalRMS = [ totalRMS ; U.en_ll(burnIn:j,2) ];
      s_ll = [ s_ll ; U.s_ll(burnIn:nthin:j)];
      if ~U.S_0.landData
        k1_ll = [ k1_ll ; U.k1_ll(burnIn:nthin:j)];
      end
      k2_ll = [ k2_ll ; U.k2_ll(burnIn:nthin:j)];
   end

   fprintf('Saving to disk...\n')
   save([FileName,'_Combined'],'s_ll','k1_ll','k2_ll','-v7.3')
   
end

