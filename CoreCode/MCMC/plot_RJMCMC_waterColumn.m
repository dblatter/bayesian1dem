function plot_RJMCMC_waterColumn(s,k,G,S)

% this function assumes that all depths are in log units; no conversion to
% linear depth will be made

nSamples = size(s,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%    defining binning and plotting arrays
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nZbins = ceil(( S.zMax - S.zMin )/G.dz);  % number of depth bins
zPlot = S.zMin+G.dz/2+(0:nZbins-1)*G.dz; %Depth axis of our PDF plots (midpoints of depth bins)

rhoBinEdges = S.rhoMin:G.drho:S.rhoMax;
nRhobins = length(rhoBinEdges) - 1;
rhoPlot = S.rhoMin + G.drho/2 + (0:nRhobins-1)*G.drho; % rho axis of our PDF plots (midpoints of rho bins)

rhoSamples = zeros(nZbins,nSamples);
kSamples = zeros(nZbins,nSamples);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%    binning
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% this is the heavy lifting bit
iProgress = 1; % for progress reporting purposes
for iSample = 1:nSamples
   x = s{iSample};
   pos = find(x.z == S.regionBoundaryDepth,1,'first');
   x.z = x.z(1:pos); % attach bottom of model to the interface vector
   iZbin=1; % for counting down the depth bins
   for iLayer=1:pos
      while x.z(iLayer) >= S.zMin + G.dz*iZbin
         rhoSamples(iZbin,iSample) = x.rhoh(iLayer);
         iZbin = iZbin + 1; % move to the next depth bin
      end
      kSamples(iZbin,iSample) = 1;
   end
   if( mod(iSample,floor(iProgress*nSamples/10)) == 0 )
      fprintf('%d %% complete...\n',floor(iProgress*10))
      iProgress = iProgress + 1;
   end
end
kSamples = kSamples(1:end-1,:); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%    making the histograms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

posteriorPDF = zeros(nZbins,nRhobins);
p5 = zeros(nZbins,1);
p95 = zeros(nZbins,1);
KLd = zeros(nZbins,1);

iProgress = 1;
for iZbin=1:nZbins
   figure(45)
   a = histogram(rhoSamples(iZbin,:),rhoBinEdges,'Normalization','pdf');
   posteriorPDF(iZbin,:) = a.Values;
   p5(iZbin) = prctile(rhoSamples(iZbin,:),5);
   p95(iZbin) = prctile(rhoSamples(iZbin,:),95);
   if( S.transform01_ab == true )
      ind = find(S.zRhoLim >= S.zMin + G.dz*iZbin,1,'first');
      if( isempty(ind) )
         ind = size(G.prior,1);
      end
      KLd(iZbin) = KLdivergence(posteriorPDF(iZbin,:),G.prior(ind,:));
   else
%       KLd(iZbin) = KLdivergence(posteriorPDF(iZbin,:),G.prior);
   end
   close 45; clear a;
   if( mod(iZbin,floor(iProgress*nZbins/10)) == 0 )
      fprintf('%d %% complete...\n',floor(iProgress*10))
      iProgress = iProgress + 1;
   end
end
p5 = [p5(1); p5(1:end-1)];
p95 = [p95(1); p95(1:end-1)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%    plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure
subplot(1,4,1)
h = pcolor(rhoPlot,zPlot,log10(posteriorPDF));
set(h,'EdgeColor','none')
hold on
stairs(p5,zPlot,'-r','linewidth',2)
stairs(p95,zPlot,'-r','linewidth',2)
if( isfield(S,'TrueModel') )
   y.z = S.TrueModel.z;
   y.rho = S.TrueModel.rho;
   plotModel1D(y)
end
xlabel('log(\rho) (ohm-m)')
ylabel('log(z) (m)')
set(gca,'YDir','reverse')
set(gca,'FontSize',14)
colorbar

subplot(1,4,2)
plot(KLd(1:end-1),zPlot(1:end-1),'linewidth',2)
set(gca,'YDir','reverse')
set(gca,'FontSize',14)
ylim([zPlot(1) zPlot(end-1)])
ylabel('log(z) (m)')
xlabel('KL divergence')

subplot(1,4,3)
kPDF = sum(kSamples,2)./(sum(sum(kSamples))*G.dz);
kPrior = mean(kPDF);
plot(kPDF,zPlot(1:end-1),'linewidth',2)
hold on
plot([kPrior kPrior],[zPlot(2) zPlot(end-1)],'--k')
set(gca,'YDir','reverse')
set(gca,'FontSize',14)
ylim([zPlot(1) zPlot(end-1)])
ylabel('log(z) (m)')
xlabel('probability density')

subplot(1,4,4)
histogram(k,'Normalization','pdf')
xlabel('# of subsurface layers')
ylabel('probability density')
set(gca,'FontSize',14)

set(gcf,'position',[210  407  1092  477])

keyboard

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%    KL divergence function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [KLd] = KLdivergence(P,Q)

if( length(P) ~= length(Q) )
   fprintf('length of input arrays must be the same\n')
   keyboard
   return
end

KLd = 0.0;

for j=1:length(P)
   if( P(j) > 0 && Q(j) > 0 )
      KLd = KLd + P(j)*(log(P(j)) - log(Q(j)));
   end
end

end
