

%get field for 1D model x 
function [AppRes, Phase, Z] = get_fieldMT(S,x,yespause)
    
% Isotropic only, ignoring rhov field

% convert back to z from log(z), if that option is enabled
if( S.logZ == true )
   x.z = 10.^(x.z);
   S.regionBoundaryDepth = 10^(S.regionBoundaryDepth);
end

%re-zero the model to z=0 at the sea floor, neglect water column
pos = find(x.z == S.regionBoundaryDepth,1,'first');
x.z = x.z(pos+1:end) - S.regionBoundaryDepth;
x.rhoh = x.rhoh(pos+1:end);

if( S.transform01_ab == true )
   if( S.logZ == true )
      S.zMin = 10^(S.zMin);
      S.zMax = 10^(S.zMax);
   end
   S.zMin = S.zMin - S.regionBoundaryDepth;
   S.zMax = S.zMax - S.regionBoundaryDepth;
   x.rho = x.rhoh; % silly naming problem
   [rhoBinned,zBinned] = Transform01_ab(x,S);
   x.rhoh = [rhoBinned rhoBinned(end)]; % add in the halfspace at the end
   x.z = zBinned;
end

rho = 10.^(x.rhoh);

%add 0 for padding
x.z = [0 x.z];
%compute layer thicknesses
h = x.z(2:end) - x.z(1:end-1);

freqs = S.MTdat.freqs;

[AppRes, Phase, Z] = MT1D(rho,h,freqs);
AppRes = log10(AppRes);

if( yespause == true )
    figure
    subplot(2,1,1)
    semilogx(1./freqs,AppRes,'bo','linestyle','-','linewidth',2);
    hold on
    semilogx(1./freqs,S.MTdat.TEappRes,'ko','linestyle','-','linewidth',2);
    title('Apparent Resistivity ')
    ylabel('ohm-m')
    set(gca,'fontsize',12)

    subplot(2,1,2)
    semilogx(1./freqs,Phase,'bo','linestyle','-','linewidth',2);
    hold on
    semilogx(1./freqs,S.MTdat.TEphase,'ko','linestyle','-','linewidth',2);    
    keyboard
end
    
end   