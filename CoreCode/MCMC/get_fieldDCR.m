function DCRresp = get_fieldDCR(S,x,yespause)

% convert to linear z from log10(z), if that option is enabled
if( S.logZ == true )
   x.z = 10.^(x.z);
   S.regionBoundaryDepth = 10.^(S.regionBoundaryDepth);
   S.zMax = 10^(S.zMax);
end

%re-zero the model to z=0 at the sea floor, neglect water column
pos = find(x.z == S.regionBoundaryDepth,1,'first');
x.z = x.z(pos+1:end) - S.regionBoundaryDepth;
x.rhoh = x.rhoh(pos+1:end);


if( S.transform01_ab == true )
   if( S.logZ == true )
      S.zMin = 10^(S.zMin);
      S.zMax = 10^(S.zMax);
   end
   S.zMin = S.zMin - S.regionBoundaryDepth;
   S.zMax = S.zMax - S.regionBoundaryDepth;
   x.rho = x.rhoh; % silly naming problem
   [rhoBinned,zBinned] = Transform01_ab(x,S);
   x.rhoh = [rhoBinned rhoBinned(end)]; % add in the halfspace at the end
   x.z = zBinned;
end

es = S.dcrDat.es; % electrode spacings

% compute layer thicknesses
x.z = [0 x.z S.zMax]; %add 0 for padding
%compute layer thicknesses
h = x.z(2:end) - x.z(1:end-1);

rho = x.rhoh;

% DCRresp = SFilt(x, t, m)';
DCRresp = SFilt(es, h, rho)';

if( sum(imag(DCRresp)~=0) )
   DCRresp = abs(DCRresp);
end

return




