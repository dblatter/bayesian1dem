
% This function takes a standard 1D trans-d layer model, with log(rho) values
% in the interval (0,1), bins them in depth according to parameters saved
% in S, and then transforms them to the interval (minRho, maxRho), also
% contained in S. The outputs are the binned transformed rho and the binned
% depths.

function [rhoBinned,zBinned] = Transform01_ab(x,S)

if( S.logZ == false ) % bin normally
   zBinned = S.zMin+(S.dz/2):S.dz:S.zMax;
else % bin in log(z)
   tmp = S.zMin+(S.dz/2):S.dz:S.zMax;
   if( S.zMin <= 0 )
      S.zMin = 1; % in case we're using log(z), in which case log(0) = undef
   end
   zBinned = logspace(log10(S.zMin),log10(S.zMax),length(tmp));
end
nz = length(zBinned);
rhoBinned = zeros(size(zBinned));

% transform to a binned model in z
for iz=1:nz
   % we're at this depth
   z_ = zBinned(iz);
   % find what the min and max rho are at this depth
   pos1 = find(S.zRhoLim > z_,1);
   if( pos1 <= 1 )
      keyboard
   end
   %linear interpolation
   local_rhoMax = S.maxRho(pos1-1) + (z_ - S.zRhoLim(pos1-1))*(S.maxRho(pos1) - S.maxRho(pos1-1))/(S.zRhoLim(pos1) - S.zRhoLim(pos1-1));
   local_rhoMin = S.minRho(pos1-1) + (z_ - S.zRhoLim(pos1-1))*(S.minRho(pos1) - S.minRho(pos1-1))/(S.zRhoLim(pos1) - S.zRhoLim(pos1-1));
   % for this depth, find the layer we're in
   pos2 = find( x.z > z_ ,1);
   if( isempty(pos2) )
      % we're beyond the last interface
      pos2 = length(x.z) + 1;
      if length(x.rho)<pos2
          pos2 = length(x.rho);
      end
   end
   % transform rho from (0,1) to (local_rhoMin,local_rhoMax)
   rhoBinned(iz) = local_rhoMin + x.rho(pos2)*(local_rhoMax - local_rhoMin);
end

end






