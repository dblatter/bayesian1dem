% gets the ocean-bottom EM field responses for a 1D model, x

function ErResponse = get_field_obCSEM(S,x,yespause)

ErResponse = zeros(length(S.obDat.Freqs),size(S.obRx.X,1),length(S.obTx.Soundings));

if( S.logZ == true )
   x.z = 10.^(x.z);
end

z = [ -100d3 ; 0.0 ; x.z' ];
rho = [ 1d12 ; 10.^(x.rhoh)' ];

for j=1:length(S.obTx.Soundings)
   Rx = [ S.obRx.X(:,j) S.obRx.Y(:,j) S.obRx.Z(:,j) ];
   Tx = [S.obTx.X S.obTx.Y S.obTx.Z S.obTx.Azimuth S.obTx.Dip];
   model = [ z rho ];
   for k=1:length(S.obDat.Freqs)
      allFields = Dipole1D(Tx,S.obDat.Freqs(k),model,Rx,0,0,S.obTx.Length,3);
      ErResponse(k,:,j) = allFields(:,5); %column 5 is the inline E-field
   end

end   

end