% Obtains the data misfit for the MCMC algorithm

function [misfit, misfitVect] = getMisfit(x,S,yespause)

if( S.debug_prior == true )
   misfit = [ 0 0 ];
   misfitVect = zeros(1,sum(S.dataTypes));
   return;
end

Chi2 = 0.0;
N = 0;
stCSEMchi2 = 0.0;
obCSEMchi2 = 0.0;
MTchi2     = 0.0;
DCRchi2    = 0.0;
misfitVect = [];

if( S.dataTypes(1) == true )
   %surface-towed CSEM data, so call Dipole1D, the forward code for this data type
   QQ = get_field_stCSEM(S,x,yespause);
   ErResponse = log10(abs(QQ));
   PhaseResponse = (180/pi)*atan2(imag(QQ),real(QQ));
   NormdErErrs = ((S.stDat.Er - ErResponse)./S.stDat.ErErr).^2;
   NormdPhaseErrs = ((S.stDat.Phase - PhaseResponse)./S.stDat.PhaseErr).^2;
   N1 = nnz(~isnan(NormdErErrs)) + nnz(~isnan(NormdPhaseErrs));
   N = N + N1;
   stCSEMchi2 = nansum(nansum(nansum(NormdErErrs))) + nansum(nansum(nansum(NormdPhaseErrs)));
   Chi2 = Chi2 + stCSEMchi2;
   misfitVect = [ misfitVect sqrt(stCSEMchi2/N1) ];
end 

if( S.dataTypes(2) == true )
   %Deep-towed CSEM
   QQ = get_field_obCSEM(S,x,yespause);
   ErResponse = log10(abs(QQ));
   PhaseResponse = (180/pi)*atan2(imag(QQ),real(QQ));
   NormdErErrs = ((S.obDat.Er - ErResponse)./S.obDat.ErErr).^2;
   NormdPhaseErrs = ((S.obDat.Phase - PhaseResponse)./S.obDat.PhaseErr).^2;
   N2 = nnz(~isnan(NormdErErrs)) + nnz(~isnan(NormdPhaseErrs));
   N = N + N2;
   obCSEMchi2 = nansum(nansum(nansum(NormdErErrs))) + nansum(nansum(nansum(NormdPhaseErrs)));
   Chi2 = Chi2 + obCSEMchi2;
   misfitVect = [ misfitVect sqrt(obCSEMchi2/N2) ];
end

if( S.dataTypes(3) == true )
   %MT data
   [MTAppRes, MTPhase, Z] = get_fieldMT(S,x,yespause);
   NormdAppResErrs = ((MTAppRes - S.MTdat.TEappRes)./S.MTdat.TEappResErr).^2;
   NormdMTphaseErrs = ((MTPhase - S.MTdat.TEphase)./S.MTdat.TEphaseErr).^2;
   N3 = length(NormdAppResErrs) + length(NormdMTphaseErrs);
   MTchi2 = sum(NormdAppResErrs) + sum(NormdMTphaseErrs);
   if( S.dataTypes(1) == true )
      Chi2 = Chi2 + MTchi2;
      N = N + N3;
   else
      Chi2 = Chi2 + MTchi2;
      N = N + N3;
   end
   misfitVect = [ misfitVect sqrt(MTchi2/N3) ];
end

if( S.dataTypes(4) == true )
   DCRappRes = get_fieldDCR(S,x,yespause);
   NormdAppResErrs = ((DCRappRes - S.dcrDat.appRes)./S.dcrDat.appResErr).^2;
   N4 = length(NormdAppResErrs);
   N = N + N4;
   DCRchi2 = sum(NormdAppResErrs);
   Chi2 = Chi2 + DCRchi2;
   misfitVect = [ misfitVect sqrt(DCRchi2/N4) ];
end

misfit = [ Chi2/2 ; sqrt((stCSEMchi2+obCSEMchi2+MTchi2+DCRchi2)/N) ];

%keyboard

if( yespause == true )
   keyboard
    figure
    subplot(2,1,1)
    semilogx(1./S.MTdat.freqs,MTAppRes,'bo','linestyle','-','linewidth',2);
    hold on
    semilogx(1./S.MTdat.freqs,S.MTdat.TEappRes,'ko','linestyle','-','linewidth',2);
    title('Apparent Resistivity ')
    ylabel('ohm-m')
    set(gca,'fontsize',12)

    subplot(2,1,2)
    semilogx(1./S.MTdat.freqs,MTPhase,'bo','linestyle','-','linewidth',2);
    hold on
    semilogx(1./S.MTdat.freqs,S.MTdat.TEphase,'ko','linestyle','-','linewidth',2);
end

end






