function PT_RJMCMC(DataFileMain,DataFile1,DataFile2,outputFolder,restart)

    %*** You need to specify these ***

    %data, frequencies used, depth of Rx, Tx, Azimuth, etc.
    S_0 = load(DataFileMain);
    S_1 = load(DataFile1);
    S_2 = load(DataFile2);
    
    if ~isfield(S_0,'landData')
        disp('****Assuming your data is marine data. If your data is land data, see Step #1 land data example and include S.landData flag in your Step 1 file.****')
        S_0.landData = false;
        S_1.landData = false;
        S_2.landData = false;
    end
    
    landData = false;
    if S_0.landData
        disp('You are using land data')
        landData = true;
        S_0.beta = -1;
    end
    
    yespause = false;
    
    %defaults which may not exist
    if isfield(S_0,'debug_prior') == 0
        S_0.debug_prior = false;
    end
    if isfield(S_0,'jeffereys_prior') == 0
        S_0.jeffereys_prior = false;
    end
    
    if ~landData
        if isfield(S_1,'birth_death_from_prior') == 0
            S_1.birth_death_from_prior = false;
        end
    end
    if isfield(S_2,'birth_death_from_prior') == 0
        S_2.birth_death_from_prior = false;
    end
    
    [~,FileRoot] = fileparts(DataFileMain);

%     %number of iterations
    N = S_0.numIterations;

    %Acceptance ratios in MCMC chains calculated every so many steps
    if isfield(S_0,'ARwindow') == 0
        ARwindow = 500;
    end
    
    %save every so many steps
    saveWindow = S_0.saveEvery;

    nDisplayEvery = S_0.displayEvery; %5d3;  % print message to screen

    %number of parallel chains (and temperatures)
    nChains = S_0.nChains;

    %inverse temperature B ladder
    B = S_0.B;

    %probability of swapping every count of the MCMC chain
    pSwap = 1;

    %*** Shouldn't need to modify below this ***

    if ~landData
        if length(S_1.UstepSize) ~= nChains || ...
            length(S_1.BstepSize) ~= nChains || ...
            length(S_1.MstepSize) ~= nChains
            beep
            disp('less steps sizes than nChains')
            return
        end
    end

    AR = cell(nChains,1);
    AR(:) = {cell(fix(N/ARwindow),1)};%Acceptance ratios
    DummyAR.uAR = 0; DummyAR.bAR = 0; DummyAR.dAR = 0; DummyAR.mAR = 0;
    DummyAR.TotalAR = 0;
    DummyAR.evalCount = 0;
    DummyAR.swapRate = 0;

    samples = cell(nChains,1);
    samples(:) = {cell(N,1)};

    kTracker1 = cell(nChains,1);
    kTracker1(:) = {zeros(N,1)};
    kTracker2 = cell(nChains,1);
    kTracker2(:) = {zeros(N,1)};

    en = zeros(N,2,nChains);% Chi^2 and RMS errors

    swapCount = cell(nChains,1);
    swapCount(:) = {zeros(N,1)};

    count = 0;
    
    if ~landData
        S1 = cell(nChains,1);
        S1(:) = {S_1};
    end
    
    S2 = cell(nChains,1);
    S1(:) = {S_1};
    S2(:) = {S_2};
    
    if nargin<5
        rng('shuffle')
        restart = false;
    else
        loadstruct = load([outputFolder,'/',FileRoot,'_PT_RJMCMC','_',num2str(nChains)]);
        loadState = loadstruct.loadState;
        RandStream.setGlobalStream(loadState.Stream)
    end
    
    %initialize stuff and start point
    for ii=1:nChains
        %need two copies of everything -- one for model 1, the other for model 2
        %model 1
        AR1{ii}(:) = {DummyAR};
        ConvStat1{ii}.uA = 0; ConvStat1{ii}.bA = 0; ConvStat1{ii}.dA = 0; ConvStat1{ii}.mA = 0;
        ConvStat1{ii}.uc = 0; ConvStat1{ii}.bc = 0; ConvStat1{ii}.dc = 0; ConvStat1{ii}.mc = 0;
        ConvStat1{ii}.evalCount = 0;
        %model 2
        AR2{ii}(:) = {DummyAR};
        ConvStat2{ii}.uA = 0; ConvStat2{ii}.bA = 0; ConvStat2{ii}.dA = 0; ConvStat2{ii}.mA = 0;
        ConvStat2{ii}.uc = 0; ConvStat2{ii}.bc = 0; ConvStat2{ii}.dc = 0; ConvStat2{ii}.mc = 0;
        ConvStat2{ii}.evalCount = 0;
        if nargin<5
            % INITIALIZE THE MODEL !!!
            if ~landData
                %first, initialize the seawater model
                %interfaces first
                k1{ii} = S_1.kInit;
                x1{ii}.z = S_1.zMin + (S_1.zMax - S_1.zMin)*rand(1,k1{ii}); %randomly choose interfaces
                x1{ii}.z = sort(x1{ii}.z);
                %if you want the interfaces fixed, set S_0.fixedDimension' == true
                %in S_0 and the interfaces of the true model will be copied in
                if( S_0.fixedDimension == true )
                   posSF = find(S_0.TrueModel.z == S_0.regionBoundaryDepth,1,'first');
                   x1{ii}.z = S_0.TrueModel.z(1:posSF-1);
                end
                %layer resistivities next
                x1{ii}.rhov = S_1.rhoMin + (S_1.rhoMax-S_1.rhoMin)*rand(1,k1{ii}+1); %randomly choose resistivities
                %if S_0.fixedWater == true, then the water column is copied in from the true model and remains fixed
                if( S_0.fixedWater == true )
                   posSF = find(S_0.TrueModel.z == S_0.regionBoundaryDepth,1,'first');
                   x1{ii}.rhov = S_0.TrueModel.rho(1:posSF);
                   S_0.beta = 0; %prevents further changes to water column
                end
                if S_1.isotropic
                    x1{ii}.rhoh = x1{ii}.rhov;
                else
                    x1{ii}.rhoh = S_1.rhoMin + (S_1.rhoMax-S_1.rhoMin)*rand(1,k1{ii}+1);
                end
            else
                x1{ii}.z = [];
                x1{ii}.rhov = [];
                x1{ii}.rhoh = [];
            end
            
            %next, initialize the subsurface model
            %interfaces first
            k2{ii} = S_2.kInit;
            x2{ii}.z = S_2.zMin + (S_2.zMax - S_2.zMin)*rand(1,k2{ii}); %randomly choose interfaces
            x2{ii}.z = sort(x2{ii}.z);
            %if you want the interfaces fixed, set S_0.fixedDimension == true
            %in S_0 and the interfaces of the true model will be copied in
            if( S_0.fixedDimension == true )
               posSF = find(S_0.TrueModel.z == S_0.regionBoundaryDepth,1,'first');
               x2{ii}.z = S_0.TrueModel.z(posSF+1:end);
            end
            %layer resistivities next
            x2{ii}.rhov = S_2.rhoMin + (S_2.rhoMax-S_2.rhoMin)*rand(1,k2{ii}+1); %randomly choose resistivities
            if S_2.isotropic
                x2{ii}.rhoh = x2{ii}.rhov;
            else
                x2{ii}.rhoh = S_2.rhoMin + (S_2.rhoMax-S_2.rhoMin)*rand(1,k2{ii}+1);
            end
            
        else      
           % restarts are not allowed---too lazy to add this functionality
        end  

        %construct the composite model from the water column and subsurface models
        sort(x1{ii}.z); sort(x2{ii}.z);
        x{ii}.z = [ x1{ii}.z S_0.regionBoundaryDepth x2{ii}.z ];
        x{ii}.rhov = [ x1{ii}.rhov x2{ii}.rhov ];
        x{ii}.rhoh = [ x1{ii}.rhoh x2{ii}.rhoh ];
        %compute the misfit of this initial model
        [oldMisfit{ii}, oldMisfitVect{ii}] = getMisfit(x{ii},S_0,yespause);
        
        %define the step sizes for each temperature (each chain)
        if ~landData
            S1{ii}.rSD1 = S_1.UstepSize(ii);
            S1{ii}.rSD2 = S_1.BstepSize(ii);
            S1{ii}.MoveSd = S_1.MstepSize(ii);
        end
        
        S2{ii}.rSD1 = S_2.UstepSize(ii);
        S2{ii}.rSD2 = S_2.BstepSize(ii);
        S2{ii}.MoveSd = S_2.MstepSize(ii);
        
    end

    callSW = 0;
    
    %start MCMC
    tic;
    tStart = tic;

    while count<N
        count = count +1;

        if mod(count,nDisplayEvery) == 0 % display text to user
           tLength      = toc(tStart);
           if restart
               aveIterRate = tLength/(count-iComputed);
           else
               aveIterRate     = tLength/count;
           end    
           predictedEnd = (N-count)*aveIterRate/86400 + now;
           fprintf('Iteration %i out of %i. Mean time per iteration: %4.5f s. Predicted completion time: %s\n',count,N,aveIterRate,datestr(predictedEnd))
           fprintf('RMS: %4.2f.   From chain T=%4.2f\n',oldMisfit{ii}(2),1/B(ii))
           fprintf('Individual RMSs: %4.2f\n',oldMisfitVect{ii})
           %keyboard
        end
        %see if swap
        if rand<pSwap

            %then swap ALL chains
            [p,q] = determinPerm(nChains);
            for iTemp = 1: nChains
              %twoInts = randperm(nChains,2);
              first = p(iTemp); second = q(iTemp);

               %now find swap probability according to likelihoods
               logAlphaSwap = (oldMisfit{first}(1) - oldMisfit{second}(1))*(B(first) - B(second));
               if log(rand)<logAlphaSwap
                  %sprintf ('%d %d\n',first, second)
                  if ~landData
                    temp_x1      = x1{first};
                    temp_k1      = k1{first};
                  end
                  
                  temp_x2      = x2{first};
                  temp_k2      = k2{first};
                  
                  if ~landData
                      x1{first}          = x1{second};
                      k1{first}          = k1{second};
                  end
                  
                  x2{first}          = x2{second};   
                  k2{first}          = k2{second};
                  
                  if ~landData
                    x1{second}         = temp_x1;
                    k1{second}         = temp_k1;
                  end
                  
                  x2{second}         = temp_x2;
                  k2{second}         = temp_k2;
                  
                  temp_misfit = oldMisfit{first};
                  oldMisfit{first}  = oldMisfit{second};
                  oldMisfit{second} = temp_misfit;                  

                  swapCount{first}(count) = 1;
                  swapCount{second}(count) = 1;
               end

            end% iTemp

        end%if pswap

        %start parallel tempering
        %one step
        for jj=1:nChains
           
            %determine whether we will modify the water column model or the subsurface model this step
            dart = rand;
            if( dart < S_0.beta ) % beta is the probability we will perturb model 1, rather than model 2
               [x1{jj},k1{jj},oldMisfit{jj},oldMisfitVect{jj},ConvStat1{jj}] = RJ_MCMC_step(x1{jj},k1{jj},x2{jj},oldMisfit{jj},oldMisfitVect{jj},ConvStat1{jj},S1{jj},S_0,B(jj),yespause);
            else
               [x2{jj},k2{jj},oldMisfit{jj},oldMisfitVect{jj},ConvStat2{jj}] = RJ_MCMC_step(x2{jj},k2{jj},x1{jj},oldMisfit{jj},oldMisfitVect{jj},ConvStat2{jj},S2{jj},S_0,B(jj),yespause);
            end

            %construct the composite model from the water column and subsurface models
            x{jj}.z = [ x1{jj}.z S_0.regionBoundaryDepth x2{jj}.z ];
            x{jj}.rhov = [ x1{jj}.rhov x2{jj}.rhov ];
            x{jj}.rhoh = [ x1{jj}.rhoh x2{jj}.rhoh ];
            %save composite model in the model ensemble: "samples"
            samples{jj}{count} = x{jj};
            if ~landData
                kTracker1{jj}(count)= k1{jj};
            end
            kTracker2{jj}(count)= k2{jj};
            en(count,:,jj)      = oldMisfit{jj};
            indivRMSs(count,:,jj)  = oldMisfitVect{jj};

            if mod(count,ARwindow) == 0
                 idx = count/ARwindow;
                 %model 1
                 AR1{jj}{idx}.uAR = ConvStat1{jj}.uA/ConvStat1{jj}.uc*100; AR1{jj}{idx}.bAR = ConvStat1{jj}.bA/ConvStat1{jj}.bc*100;
                 AR1{jj}{idx}.dAR = ConvStat1{jj}.dA/ConvStat1{jj}.dc*100; AR1{jj}{idx}.mAR = ConvStat1{jj}.mA/ConvStat1{jj}.mc*100;
                 AR1{jj}{idx}.TotalAR = (ConvStat1{jj}.uA + ConvStat1{jj}.bA + ConvStat1{jj}.dA + ConvStat1{jj}.mA)/ARwindow*100;
                 AR1{jj}{idx}.evalCount = ConvStat1{jj}.evalCount;
                 AR1{jj}{idx}.swapRate  = sum(swapCount{jj}(count-ARwindow+1:count))/ARwindow*100;
                 ConvStat1{jj}.uA = 0; ConvStat1{jj}.bA = 0; ConvStat1{jj}.dA = 0; ConvStat1{jj}.mA = 0;
                 ConvStat1{jj}.uc = 0; ConvStat1{jj}.bc = 0; ConvStat1{jj}.dc = 0; ConvStat1{jj}.mc = 0;
                 %model 2
                 AR2{jj}{idx}.uAR = ConvStat2{jj}.uA/ConvStat2{jj}.uc*100; AR2{jj}{idx}.bAR = ConvStat2{jj}.bA/ConvStat2{jj}.bc*100;
                 AR2{jj}{idx}.dAR = ConvStat2{jj}.dA/ConvStat2{jj}.dc*100; AR2{jj}{idx}.mAR = ConvStat2{jj}.mA/ConvStat2{jj}.mc*100;
                 AR2{jj}{idx}.TotalAR = (ConvStat2{jj}.uA + ConvStat2{jj}.bA + ConvStat2{jj}.dA + ConvStat2{jj}.mA)/ARwindow*100;
                 AR2{jj}{idx}.evalCount = ConvStat2{jj}.evalCount;
                 AR2{jj}{idx}.swapRate  = sum(swapCount{jj}(count-ARwindow+1:count))/ARwindow*100;
                 ConvStat2{jj}.uA = 0; ConvStat2{jj}.bA = 0; ConvStat2{jj}.dA = 0; ConvStat2{jj}.mA = 0;
                 ConvStat2{jj}.uc = 0; ConvStat2{jj}.bc = 0; ConvStat2{jj}.dc = 0; ConvStat2{jj}.mc = 0;
            end
        end%one step

        %see if time to save
        if mod(count,saveWindow)==0 || count == N
           for ll = 1:nChains
            loadState.x{ll} = x{ll};
            loadState.Stream = RandStream.getGlobalStream;
            s_ll = samples{ll};  en_ll = en(:,:,ll); AR1_ll = AR1{ll}; 
                                k2_ll = kTracker2{ll};                     AR2_ll = AR2{ll}; S2_ll=S2{ll};
            if ~landData
                S1_ll=S1{ll};
                k1_ll = kTracker1{ll};
            else
                S1_ll = NaN;
                k1_ll = NaN;
            end
            indivRMSs_ll = indivRMSs(:,:,ll);
            save ([outputFolder,'/',FileRoot,'_PT_RJMCMC','_',num2str(ll)],'S1_ll','s_ll','k1_ll','en_ll', 'AR1_ll',...
                                                                           'k2_ll','AR2_ll','S2_ll','S_0','loadState',...
                                                                           'indivRMSs_ll')
           end
          save ([outputFolder,'/',FileRoot,'_PT_RJMCMC','_swaps'],'swapCount')
        end

    end
end


%RJ MCMC moves

function [pertNorm,xNew,priorViolate]=birth(k,x,S)
    xNew.z   = zeros(1,k+1) +NaN;%fills a new model with NaNs
    xNew.rhoh = zeros(1,k+1+1) +NaN;
    xNew.rhov = zeros(1,k+1+1) +NaN;
    pertNorm=0;
    priorViolate=0;

    %propose a new layer interface, at a non-existent interface location
    zProp = S.zMin + rand*(S.zMax-S.zMin);
    %get the index of the interface directly ABOVE the new one
    [~,pos]=ismember(0,(x.z >= zProp),'legacy'); 

    % draw samples from the proposal
    if( S.birth_death_from_prior == true ) % the proposal = the prior
       
       unifDraw = rand(1,2);
       
       if S.isotropic
          unifDraw(1) = unifDraw(2);
       end
       
       newRho = S.rhoMin + unifDraw*(S.rhoMax - S.rhoMin);
       
    else % the proposal is a normal distribution centered at the current rho value
    
       mu       = [x.rhoh(pos+1),x.rhov(pos+1)];
       normDraw = randn(1,2);

       if S.isotropic%just to make sure no priorviolation takes place
         normDraw(1) = normDraw(2);%and pertnorm is isotropic
         mu(1) = mu(2);
       end

       newRho   = mu + normDraw*[S.rSD2 0;0 S.rSD2];
       
       %now actually get the model pert norm sq
       pertNorm=sum((normDraw).^2);
       
    end

    % check if we have violated the prior distribution
    if  newRho(1) < S.rhoMin || newRho(1) > S.rhoMax || ...
        newRho(2) < S.rhoMin || newRho(2) > S.rhoMax
        priorViolate=1;
        return %exit from this function with priorViolate=1
    end
    
    %positions for copying old values into new model vector
    xNew.z(pos+1)=zProp;
    xNew.z(isnan(xNew.z))=x.z;
    if rand<0.5
        pos=pos+1;
    end
    xNew.rhoh(pos+1)=newRho(1);
    xNew.rhoh(isnan(xNew.rhoh))=x.rhoh;
    xNew.rhov(pos+1)=newRho(2);
    xNew.rhov(isnan(xNew.rhov))=x.rhov;

end

function [xNew,priorViolate] = move (x,k,S)

    %pick an interface between 1 and current k to move
    l = randi(k);
    zProp = x.z(l) + S.MoveSd*randn;
    if zProp<S.zMin || zProp>S.zMax
        xNew=x;
        priorViolate=1;
        return; %outside bounds of z
    end

    priorViolate = 0;%checks passed!!
    %See where we are
    [~,pos]=ismember(0,(x.z(1:k)>=zProp),'legacy');
    if pos==l || pos == l-1
        %things are fine, nothing needs to be shifted
        xNew=x;
        xNew.z(l)=zProp;
        return;
    else %move the interface depth and properties
       x.z(l)  = [];%remove the interface
        k=k-1;
        if rand<0.5
            l=l+1;
        end
        %delete rho either above or below this interface
        %keep a copy of deleted value to move
        temp_rhoh    = x.rhoh(l);
        x.rhoh(l) = [];
        temp_rhov    = x.rhov(l);
        x.rhov(l) = [];
        %now birth the interface at zProp
        xNew.z   = zeros(1,k+1) +NaN;%fills a new model with NaNs
        xNew.rhoh = zeros(1,k+1+1) +NaN;
        xNew.rhov = zeros(1,k+1+1) +NaN;

        [~,pos]=ismember(0,(x.z(1:k)>=zProp),'legacy');
        %find where we are again just to be safe
        xNew.z(pos+1)=zProp;
        xNew.z(isnan(xNew.z))=x.z;

        if rand<0.5
            pos=pos+1;
        end
        xNew.rhoh(pos+1)=temp_rhoh;
        xNew.rhoh(isnan(xNew.rhoh))=x.rhoh;
        xNew.rhov(pos+1)=temp_rhov;
        xNew.rhov(isnan(xNew.rhov))=x.rhov;

        k=k+1;%not that this matters, we're not returning it.

    end

end

function [pertNorm,xNew] = death (x,k,S)
    xNew=x;
    %pick an interface to delete
    l = ceil(rand*(k));
    if S.isotropic%just to make sure H V perturbations are same
      x.rhoh(l)   = x.rhov(l);
      x.rhoh(l+1) = x.rhoh(l+1);
    end
    %these are the perturbations
    pertNorm = sum(([x.rhoh(l)-x.rhoh(l+1),x.rhov(l)-x.rhov(l+1)]/S.rSD2).^2);
    xNew.z(l)  = [];
    if rand<0.5
        l=l+1;
    end
    xNew.rhoh(l)= [];
    xNew.rhov(l)= [];
 end

function [xNew,priorViolate]=rhoUpdate(k,x,S,smallFlag)
    if strcmp(smallFlag,'small')
        S.rSD1 = S.rSD1/S.localFac;
    end
    xNew=x;
    priorViolate = 0;
    muH       = [x.rhoh];
    muV       = [x.rhov];
    normDrawH = randn(1,k+1);
    normDrawV = randn(1,k+1);
    newRhoH   = muH + normDrawH*(S.rSD1*eye(k+1));
    newRhoV   = muV + normDrawV*(S.rSD1*eye(k+1));
    if S.isotropic%just to make sure no priorviolation takes place
	newRhoH = newRhoV;
    end
    if  any(newRhoH < S.rhoMin) || any(newRhoH > S.rhoMax) % || ...
        %any(newRhoV < S.rvMin) || any(newRhoV > S.rvMax)
        priorViolate=1;
        return %exit from this function with priorViolate=1
    end
    xNew.rhoh=newRhoH;
    xNew.rhov=newRhoV;
end

%end RJMCMC moves

function [p,q] = determinPerm(n)
%returns index pairs p,q form strictly upper triangular matrix
    k = randperm(n/2*(n-1));
    q = floor(sqrt(8*(k-1) + 1)/2 + 3/2);
    p = k - (q-1).*(q-2)/2;
end

%RJ MCMC main step
function [x,k,oldMisfit,oldMisfitVect,ConvStat] = RJ_MCMC_step (x,k,x_passive,oldMisfit,oldMisfitVect,ConvStat,S,S_0,B,yespause)
        del = S.rhoMax - S.rhoMin;
        %roll the dice
        dart=rand();
        CDF=[1/4,2/4,3/4,4/4];
        [~,pos]=ismember(0,(CDF>=dart),'legacy');
        pos=pos+1;%we know where it fell now
        priorViolate=0;
        if( S_0.fixedDimension == true )
           pos = 4; % prohibit all moves except 'update'
        end
        switch pos
            case 1 %birth
                ConvStat.bc=ConvStat.bc+1;
                if k==S.kMax
                    priorViolate=1; %no birth allowed
                end
                if(~priorViolate)
                    [pertNorm,xNew,priorViolate]=birth(k,x,S);
                end
                %calculate acceptance probability
                if (~priorViolate)
                    %form complete model to calculate misfit
                    if( x.z(end) < S_0.regionBoundaryDepth )
                       x_composite.z = [ xNew.z S_0.regionBoundaryDepth x_passive.z ];
                       x_composite.rhov = [ xNew.rhov x_passive.rhov ];
                       x_composite.rhoh = [ xNew.rhov x_passive.rhoh ];
                    else
                       x_composite.z = [ x_passive.z S_0.regionBoundaryDepth xNew.z ];
                       x_composite.rhov = [ x_passive.rhov xNew.rhov ];
                       x_composite.rhoh = [ x_passive.rhoh xNew.rhov ];
                    end
                    % compute the proposal model data misfit (forward call here)        
                    [newMisfit,newMisfitVect] = getMisfit(x_composite,S_0,yespause);
                    ConvStat.evalCount = ConvStat.evalCount+1;
                    if( S.birth_death_from_prior == false ) % proposal is Gaussian
                       %just priorR times propR
                       logalpha  = 2*log(S.rSD2)+log(2*pi)-2*log(del)+(pertNorm/2);
                       if S.isotropic%then priorR times propR is sqrtd
                           logalpha = 0.5*logalpha;
                       end
                       %insert likelihood to get alpha
                       if S_0.jeffereys_prior
                           logalpha  = logalpha + log(k) - log(k+1);
                       end    
                       logalpha  = logalpha -(newMisfit(1) - oldMisfit(1))*B;
                    else % proposal = prior, so alpha is just ratio of likelihoods
                       logalpha = -(newMisfit(1) - oldMisfit(1))*B;
                    end
                else
                    logalpha=-Inf;%prior has been violated so reject model
                end
                if log(rand)<logalpha %accept model
                    k=k+1;
                    x=xNew;
                    oldMisfit = newMisfit;
                    oldMisfitVect = newMisfitVect;
                    ConvStat.bA = ConvStat.bA +1;
                end

            case 2 %death
                ConvStat.dc=ConvStat.dc+1;
                if (k==S.kMin)
                    priorViolate=1; %no death at kMin
                end
                if (~priorViolate)
                    [pertNorm,xNew] = death (x,k,S);
                    %form complete model to calculate misfit
                    if( x.z(end) < S_0.regionBoundaryDepth )
                       x_composite.z = [ xNew.z S_0.regionBoundaryDepth x_passive.z ];
                       x_composite.rhov = [ xNew.rhov x_passive.rhov ];
                       x_composite.rhoh = [ xNew.rhov x_passive.rhoh ];
                    else
                       x_composite.z = [ x_passive.z S_0.regionBoundaryDepth xNew.z ];
                       x_composite.rhov = [ x_passive.rhov xNew.rhov ];
                       x_composite.rhoh = [ x_passive.rhoh xNew.rhov ];
                    end
                    %compute acceptance
                    % first, get the proposal model data misfit (forward call here)
                    [newMisfit,newMisfitVect] = getMisfit(x_composite,S_0,yespause);
                    ConvStat.evalCount = ConvStat.evalCount +1;
                    if( S.birth_death_from_prior == false ) % proposal is Gaussian
                       %just priorR times propR
                       logalpha = -2*log(S.rSD2)-log(2*pi)+2*log(del)-(pertNorm/2);
                       if S.isotropic%then priorR times propR is sqrtd
                           logalpha = 0.5*logalpha;
                       end
                       %insert likelihood to get alpha
                       if S_0.jeffereys_prior
                           logalpha  = logalpha + log(k) - log(k-1);
                       end    
                       logalpha  = logalpha -(newMisfit(1) - oldMisfit(1))*B;
                    else % proposal = prior, so alpha is just ratio of likelihoods
                       logalpha = -(newMisfit(1) - oldMisfit(1))*B;
                    end
                else
                    logalpha=-Inf;
                end
                if log(rand)<logalpha %accept model
                    k=k-1;
                    x=xNew;
                    oldMisfit = newMisfit;
                    oldMisfitVect = newMisfitVect;
                    ConvStat.dA = ConvStat.dA +1;
                end

            case 3%move
                ConvStat.mc=ConvStat.mc+1;
                [xNew,priorViolate] = move (x,k,S);
                if ~priorViolate
                    %form complete model to calculate misfit
                    if( x.z(end) < S_0.regionBoundaryDepth )
                       x_composite.z = [ xNew.z S_0.regionBoundaryDepth x_passive.z ];
                       x_composite.rhov = [ xNew.rhov x_passive.rhov ];
                       x_composite.rhoh = [ xNew.rhov x_passive.rhoh ];
                    else
                       x_composite.z = [ x_passive.z S_0.regionBoundaryDepth xNew.z ];
                       x_composite.rhov = [ x_passive.rhov xNew.rhov ];
                       x_composite.rhoh = [ x_passive.rhoh xNew.rhov ];
                    end
                    [newMisfit,newMisfitVect] = getMisfit(x_composite,S_0,yespause);
                    ConvStat.evalCount = ConvStat.evalCount +1;
                    logalpha = -(newMisfit(1) - oldMisfit(1))*B;
                else
                    logalpha=-Inf;
                end
                if log(rand)<logalpha %accept model
                    %k remains the same
                    x=xNew;
                    oldMisfit = newMisfit;
                    oldMisfitVect = newMisfitVect;
                    ConvStat.mA = ConvStat.mA +1;
                end

            case 4%update
                ConvStat.uc=ConvStat.uc+1;
                [xNew,priorViolate]=rhoUpdate(k,x,S,'large');%always large in PT
                if ~priorViolate
                   %form complete model to calculate misfit
                    if( x.z(end) < S_0.regionBoundaryDepth )
                       x_composite.z = [ xNew.z S_0.regionBoundaryDepth x_passive.z ];
                       x_composite.rhov = [ xNew.rhov x_passive.rhov ];
                       x_composite.rhoh = [ xNew.rhov x_passive.rhoh ];
                    else
                       x_composite.z = [ x_passive.z S_0.regionBoundaryDepth xNew.z ];
                       x_composite.rhov = [ x_passive.rhov xNew.rhov ];
                       x_composite.rhoh = [ x_passive.rhoh xNew.rhov ];
                    end
                    [newMisfit,newMisfitVect] = getMisfit(x_composite,S_0,yespause);
                    ConvStat.evalCount = ConvStat.evalCount +1;
                    logalpha = -(newMisfit(1) - oldMisfit(1))*B;
                else
                    logalpha=-Inf;
                end
                if log(rand)<logalpha %accept model
                    %k remains the same
                    x=xNew;
                    oldMisfit = newMisfit;
                    oldMisfitVect = newMisfitVect;
                    ConvStat.uA  = ConvStat.uA +1;
                end

        end% all moves switch
        
end
%end RJ MCMC step
