% gets the surface-towed EM field responses for a 1D model, x

function ErResponse = get_field_stCSEM(S,x,yespause)

ErResponse = zeros(length(S.stDat.Freqs),size(S.stRx.X,1),length(S.stTx.Soundings));

if( S.logZ == true )
   x.z = 10.^(x.z);
end

z = [ -100d3 ; 0.0 ; x.z' ];
rho = [ 1d12 ; 10.^(x.rhoh)' ];

for j=1:length(S.stTx.Soundings)
   Rx = [ S.stRx.X(:,j) S.stRx.Y(:,j) S.stRx.Z(:,j) ];
   Tx = [ S.stTx.X(j) S.stTx.Y(j) S.stTx.Z(j) S.stTx.Azimuth S.stTx.Dip ];
   model = [ z rho ];
   for k=1:length(S.stDat.Freqs)
      allFields = Dipole1D(Tx,S.stDat.Freqs(k),model,Rx,0,0,S.stTx.Length,3);
      ErResponse(k,:,j) = allFields(:,5); %column 5 is the inline E-field
   end
end   

end

