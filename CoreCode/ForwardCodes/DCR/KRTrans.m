function trans = KRTrans(lam, thick, rho)
% computes the Koefoed resistivity transform
% usage:
%     trans = KRTrans(lam, thick, rho)
% modified to work with log resistivities as imput 

n = length(rho);
trans = 10^rho(n);
for i = n-1:-1:1
  tlt = tanh(thick(i)*lam);
  trans = (trans + 10^rho(i)*tlt)/(1+trans*tlt/10^rho(i));
end
return