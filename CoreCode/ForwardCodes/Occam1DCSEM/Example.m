 

%
% Receiver positions (x,y,z) m:
%
% Note receivers positioned along y-axis to be similar to geometry in 2D
% data files
% 
Rx = [  0 607.8     1;  
        0  867.8    1;
        0 1123.7    1;
        0 1384.3    1];

%
% Transmitter:
%
% X, Y, Z, Azimuth, Dip
%
% Note: azimuth = 90 means dipole pointing along y axis, like geometry in
% 2D data file
%

Tx = [0 0 1 90 0];
TxLength =  336.9; % length for freshwater survey off NJ 


% 
% Models:
%
% [Depth of top of layer (m) , resistivity (ohm-m)] First layer is air
% layer.

Model1 = [    -100e3 1d12; 
                  0  0.2;
                  30 0.2 
                 100  1];
             
Model2 = [    -100e3 1d12; 
                  0  0.2;
                  30 0.3 
                 100  1]; 
             
Model3 = [    -100e3 1d12; 
                  0  0.3;
                  30 0.2 
                 100  1];              
 

    
frequencies = logspace(-1,2,50); % wide range for studying what happens when seawater conductivity changes

%---
%
% Compute EM response using Dipole1D code:
%

% Initialize memory for response arrays:

[Ey1,Ey2,Ey3] = deal(zeros(size(Rx,1),length(frequencies)));
 
for iFreq = 1:length(frequencies)
    nFields1 = Dipole1D(Tx,frequencies(iFreq),Model1,Rx,0,0,TxLength);
    nFields2 = Dipole1D(Tx,frequencies(iFreq),Model2,Rx,0,0,TxLength);
    nFields3 = Dipole1D(Tx,frequencies(iFreq),Model3,Rx,0,0,TxLength);
    
    Ey1(:,iFreq) = nFields1(:,5);
    Ey2(:,iFreq) = nFields2(:,5);
    Ey3(:,iFreq) = nFields3(:,5);
end

%---
% Plot results:



figure;
h1 = loglog(frequencies,abs(Ey1),'b-');
hold all;
h2 = loglog(frequencies,abs(Ey2),'b--');
h3 = loglog(frequencies,abs(Ey3),'b:');

fontSize = 14; 
set(gca,'fontsize',fontSize);

lineWidth = 1;
hLines = findobj(gcf,'type','line');
set(hLines,'linewidth',lineWidth)

legend([h1(1) h2(1) h3(1)],{'Model 1','Model 2','Model 3'})
xlabel('Frequency (Hz)')
ylabel('Amplitude (V/Am^2)')

 
