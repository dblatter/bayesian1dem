

% Step 4:
% Combine the post-burnin samples from all the T=1 chains into
% one combined model ensemble .mat file for plotting the posterior
% distribution

% The inputs to this function are
% 1: the filename of each of the PT_RJMCMC output files, except for
%    the last '_n.mat' (the part that is identical to all of them)
% 2: the burn-in; all the models from 1:burnIn-1 will be discarded,
%    while all the models burnIn:end will be included
% 3: the number to decimate by (large files are cumbersome, and the key
%    to a good approximation to the posterior is long chains, not
%    just lots of models
% 4: the total number of PT chains
% 5: the number of PT chains at T=1

%FileNameRoot = 'Line1_Joint_ST211-225_MT5';
%FileNameRoot = 'synthetic_obCSEM';
FileNameRoot = 'SchlumbergerDCR';
%FileNameRoot = 'SERPENT_S13_MT';
%FileNameRoot = 'RIS_MT_A24';
%FileNameRoot = 'Land_MTData';

FileName = ['Trash/',FileNameRoot,'_PT_RJMCMC'];
burnIn = 3d2;
nthin = 1;
nChains = 8;
nChainsAtOne = 3;

MT = 0;     %set this to '1' if you inverted MT data
DCR = 1;    %set this to '1' if you inverted DC resistivity data
stCSEM = 0;   %set this to '1' if you inverted surface-towed CSEM data
obCSEM = 0; %set this to '1' if you inverted ocean-bottom CSEM data

totalRMS = CombineChains(FileName,burnIn,nthin,nChains,nChainsAtOne);

fprintf('Plotting model responses...\n')
PlotCSEM_MT_ModelResponsesAndData(['Trash/',FileNameRoot],MT,DCR,stCSEM,obCSEM)

fprintf('Making histogram of datafit across all models in the ensemble...\n')
figure(3)
histogram(totalRMS,'normalization','pdf','edgecolor','none')
xlabel('RMS misfit')
ylabel('probability density')
set(gca,'fontsize',14)
