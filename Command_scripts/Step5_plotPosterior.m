 
%
% Read in PT_RJMCMC results and plot them up:
%

clear
close all

%Factor by which to thin the model ensemble before posterior estimation
%Because length of chains is more important than total models, a subsample
%of the total model ensemble is sufficient to estimate the posterior, if
%the individual chains are long enough
thin   = 10;

%FileNameRoot = 'Line1_Joint_ST211-225_MT5';
%FileNameRoot = 'synthetic_obCSEM';
FileNameRoot = 'SchlumbergerDCR';
%FileNameRoot = 'SERPENT_S13_MT';
%FileNameRoot = 'RIS_MT_A24';
%FileNameRoot = 'Land_MTData';


% Binning in depth and resistivity
% Resistivity is always sampled in log units. For depth, if in Step 1 you
% set S.logZ = true, then G.dz is in log units. Otherwise, it is linear (m)
% Be sure to set the value of G.dz below accordingly, or it this step might
% take A VERY LONG TIME to run.
G.dz = 0.05; % in log10(z) (m)
G.drho = 0.05; % in log10(rho) (ohm-m)

%-------------------------------------------------------------------------------------
% Don't worry about anything below here
%-------------------------------------------------------------------------------------

targetFolder = 'Trash/';
DataFile = strcat(targetFolder,[FileNameRoot,'.mat']);
DataFileS1 = strcat(targetFolder,'WaterColumn.mat');
DataFileS2 = strcat(targetFolder,'Subsurface.mat');

S = load(DataFile);
S1 = load(DataFileS1);
S2 = load(DataFileS2);
[~,FileRoot] = fileparts(DataFile);

display('loading model ensemble...')
load(strcat(targetFolder,FileRoot,'_PT_RJMCMC_Combined.mat')) %if PT and multiple chains at T=1
display('finished loading model ensemble!')

k1_ll = k1_ll(1:thin:end); k2_ll = k2_ll(1:thin:end);
k_old = [ k1_ll k2_ll ];
k = k2_ll;
s = s_ll(1:thin:end);

if ~isfield(S,'landData')
    S.landData = false;
end

if ~S.landData
   S1.zMin = 0;
end

% define the prior (perhaps depth-dependent) so we can calculate KL divergence
if( S.transform01_ab == true )
   rho = S.minRho:G.drho:S.maxRho;
   nRho = length(rho) - 1;
   G.prior = zeros(length(S2.rhoMin),nRho);
   for j=1:length(S.minRho)
      ind1 = find(rho >= S.minRho(j),1,'first');
      ind2 = find(rho >= S.maxRho(j),1,'first') - 1;
      G.prior(j,ind1:ind2) = (1/(S.maxRho(j)-S.minRho(j))) * ones(ind2-ind1+1,1);
   end
else
   %rho1 = min(S1.rhoMin,S2.rhoMin); rho2 = max(S1.rhoMax,S2.rhoMax);
   rho = S2.rhoMin:G.drho:S2.rhoMax;
   nRho = length(rho) - 1;
   G.prior = (1/(S2.rhoMax-S2.rhoMin)) * ones(nRho,1);
end

% mins and maxes for the depth and rho axes
if( S.transform01_ab == true )
   S.rhoMin = min(S.minRho);
   S.rhoMax = max(S.maxRho);
   S.zMax = S2.zMax;
   S.zMin = S2.zMin;
   S.zRhoLim = log10(S.zRhoLim); % make sure this is in log depth, not linear depth
else
   S.rhoMin = S2.rhoMin;
   S.rhoMax = S2.rhoMax;
   S.zMax = S2.zMax;
   S.zMin = S1.zMin;
end


plot_RJMCMC(s,k,G,S)
