

%
% Call the RJMCMC code: This will take awhile!
%

clear all
clc
%FileNameRoot = 'Line1_Joint_ST211-225_MT5';
%FileNameRoot = 'synthetic_obCSEM';
FileNameRoot = 'SchlumbergerDCR';
%FileNameRoot = 'SERPENT_S13_MT';
%FileNameRoot = 'RIS_MT_A24';
%FileNameRoot = 'Land_MTdata';

DataFileMain = ['Trash/',FileNameRoot,'.mat'];
DataFile1 = 'Trash/WaterColumn.mat';
DataFile2 = 'Trash/Subsurface.mat';
outputFolder = 'Trash';
yesRestart = false;

PT_RJMCMC(DataFileMain,DataFile1,DataFile2,outputFolder)
disp('At last, done with PT_RJ_MCMC. Proceed to Step 3');
