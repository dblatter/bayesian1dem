

% Step 3:
% Plot the misfit and other parameters as a function of model number
% This is important for determining the length of the "burn-in" as
% well as evaluating convergence to the posterior
%

%FileNameRoot = 'Line1_Joint_ST211-225_MT5';
%FileNameRoot = 'synthetic_obCSEM';
FileNameRoot = 'SchlumbergerDCR';
%FileNameRoot = 'SERPENT_S13_MT';
%FileNameRoot = 'RIS_MT_A24';
%FileNameRoot = 'Land_MTData';

nChains = 8;

plot_convergence_PT_RJMCMC(['Trash/',FileNameRoot],nChains)
